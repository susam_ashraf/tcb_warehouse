import React from 'react'
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles'
// layout for this page
import Admin from 'layouts/Admin.js'
// core components
import GridItem from 'components/Grid/GridItem.js'
import GridContainer from 'components/Grid/GridContainer.js'
import Table from 'components/Table/Table.js'
import Card from 'components/Card/Card.js'
import CardHeader from 'components/Card/CardHeader.js'
import CardBody from 'components/Card/CardBody.js'
import CardFooter from 'components/Card/CardFooter.js'
import Button from 'components/CustomButtons/Button.js'
import Link from 'next/link'
import axios from 'axios'
import { useEffect, useState } from 'react'
import stylesCustom from './dealers.module.css'
import Router, { withRouter } from 'next/router'
import Cookies from 'js-cookie'
import { BASE_URL } from '../../env.js'
import Chip from '@material-ui/core/Chip'
import FaceIcon from '@material-ui/icons/Face'
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord'
import stylesCustomAllotment from './add-new-allotment.module.css'
import TextField from '@material-ui/core/TextField'

import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import DeleteIcon from '@material-ui/icons/Delete'
import CancelIcon from '@material-ui/icons/Cancel'
import WithAuth from '../../components/WithAuth'
import Pagination from '@material-ui/lab/Pagination'
import SearchIcon from '@material-ui/icons/Search'
import InputBase from '@material-ui/core/InputBase'
import CircularProgress from '@material-ui/core/CircularProgress'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'

// const useStylesModal = makeStyles(theme => ({
//   modal: {
//     display: 'flex',
//     alignItems: 'center',
//     justifyContent: 'center'
//   },
//   paper: {
//     backgroundColor: theme.palette.background.paper,
//     border: '2px solid #000',
//     boxShadow: theme.shadows[5],
//     padding: theme.spacing(2, 4, 3)
//   }
// }))

const useStylesModal = makeStyles(theme => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    width: '85vw',
    height: '90vh',
    overflow: 'scroll'
  },
  paperSave: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  }
}))

function Dealers () {
  const classesModal = useStylesModal()
  const [open, setOpen] = React.useState(false)
  const [deleteID, setDeleteID] = React.useState(99)

  const handleOpenProductRemoveModal = index => {
    setOpenProductRemoveModal(true)
  }

  const handleCloseProductRemoveModal = () => {
    setOpenProductRemoveModal(false)
  }
  const handleOpen = id => {
    setOpen(true)
    setDeleteID(id)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const useStyles = makeStyles(theme => ({
    cardCategoryWhite: {
      '&,& a,& a:hover,& a:focus': {
        color: 'rgba(255,255,255,.62)',
        margin: '0',
        fontSize: '14px',
        marginTop: '0',
        marginBottom: '0'
      },
      '& a,& a:hover,& a:focus': {
        color: '#FFFFFF'
      }
    },
    cardTitleWhite: {
      color: '#FFFFFF',
      marginTop: '0px',
      minHeight: 'auto',
      fontWeight: '300',
      fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
      marginBottom: '3px',
      textDecoration: 'none',
      '& small': {
        color: '#777',
        fontSize: '65%',
        fontWeight: '400',
        lineHeight: '1'
      }
    },
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      // backgroundColor: alpha(theme.palette.common.white, 0.15),
      // '&:hover': {
      //   backgroundColor: alpha(theme.palette.common.white, 0.25),
      // },
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto'
      },
      border: '1px solid #D3D3D3'
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    inputRoot: {
      color: 'inherit'
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '20ch'
      },
      marginTop: '8px'
    }
  }))
  const classes = useStyles()

  const [resDatas, setResDatas] = useState([])
  const [refreshDeleteFlag, setRefreshDeleteFlag] = useState(true)
  const [localToken, setLocalToken] = useState()
  const [page, setPage] = useState(1)
  const [lastPage, setLastPage] = useState(1)
  const [searchKey, setSearchKey] = useState('')
  const [warehouseOtp, setWarehouseOtp] = useState('')
  const [dealerList, setDealerList] = useState([])
  const [openProductRemoveModal, setOpenProductRemoveModal] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [isCorrectOtp, setIsCorrectOtp] = useState(false)
  const [selectedAllotmentId, setSelectedAllotmentId] = useState()

  const handlePaginaton = (event, value) => {
    setPage(value)

    let paginationData = axios({
      url: `${BASE_URL}/api/products?page=${value}`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${Cookies.get('cToken')}`
      },
      // data: submitData,
      method: 'get'
    })
      .then(response => {
        // setResDatas(response.data);
        console.log('pagnate Data print----- : ', response)

        response.data.data.map(item =>
          // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

          buffer.push([
            // <Link
            //   href={`/admin/details-dealer?id=${item.id}`}
            //   className={stylesCustom.blackC}
            // >
            //   {item.name}
            // </Link>,
            item.dealer_id,
            item.name,
            item.mobile_no,
            item.shop_name,
            item.upazila,
            `${item.distance_from_zonal} km`,
            item.contract_status,
            <Link href={`/admin/edit-dealer?id=${item.id}`}>
              <button
                class='MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss95 jss111 jss412'
                tabindex='0'
                type='button'
              >
                <svg
                  style={{ color: '#4caf50' }}
                  class='MuiSvgIcon-root jss413'
                  focusable='false'
                  viewBox='0 0 24 24'
                  aria-hidden='true'
                >
                  <path d='M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34a.9959.9959 0 00-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z'></path>
                </svg>
              </button>
            </Link>,
            <button
              onClick={() => handleOpen(item.id)}
              class='MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss97 jss111 jss412'
              tabindex='0'
              type='button'
            >
              <svg
                style={{ color: '#f44336' }}
                class='MuiSvgIcon-root jss413'
                focusable='false'
                viewBox='0 0 24 24'
                aria-hidden='true'
              >
                <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z'></path>
              </svg>
            </button>
            // <div>
            //   <button type='button' onClick={() => handleOpen(item.id)}>
            //     react-transition-group
            //   </button>
            // </div>
          ])
        )
        setResDatas(buffer)

        console.log(resDatas, '---buffer==')

        // return Router.push('/admin/table-list')
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch(err => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        console.log('token print----- : ', err)
      })
  }

  const handleCheckOtp = index => {
    return (
      axios({
        url: `${BASE_URL}/api/warehouse/check-otp?dealerId=${dealerList[index][6]}&allotmentId=${dealerList[index][2]}&otp=${warehouseOtp}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        // data: submitData,
        method: 'get'
      })
        .then(response => {
          // setResDatas(response.data);
          console.log('OTP Data print----- : ', response.data)

          setTimeout(() => {
            setIsLoading(false)
            setIsCorrectOtp(true)
          }, 1000)

          // response.data.map(item =>
          //   // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

          //   bufferDealer.push([
          //     item.dealer_id,
          //     item.name,
          //     item.mobile_no,
          //     item.shop_name,
          //     item.upazila,
          //     item.contract_status,
          //     item.contract_expiry_date,
          //     item.family_dealer_id
          //   ])
          // )

          // setDealerList(bufferDealer)

          // console.log('---Dealer buffer--- :', dealerList)
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          setTimeout(() => {
            setIsLoading(false)
            setIsCorrectOtp(false)
          }, 1000)
          console.log('OTP Data error print----- : ', err)
        })
    )
  }

  let buffer = []

  // let localToken

  // useEffect(() => {
  //   if (!Cookies.get('cToken') || Cookies.get('cToken') == undefined) {
  //     return Router.push('/admin/login')
  //   }
  // }, [])

  useEffect(() => {
    let initialData = axios({
      url: `${BASE_URL}/api/products`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${Cookies.get('cToken')}`
      },
      // data: submitData,
      method: 'get'
    })
      .then(response => {
        // setResDatas(response.data);
        // console.log('Data print----- : ', response.data)

        setLastPage(response.data.last_page)

        response.data.data.map(item =>
          // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

          buffer.push([
            // <Link
            //   href={`/admin/details-dealer?id=${item.id}`}
            //   className={stylesCustom.blackC}
            // >
            //   {item.name}
            // </Link>,
            item.dealer_id,
            item.name,
            item.mobile_no,
            item.shop_name,
            item.upazila,
            `${item.distance_from_zonal} km`,
            item.contract_status,
            <Link href={`/admin/edit-dealer?id=${item.id}`}>
              <button
                class='MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss95 jss111 jss412'
                tabindex='0'
                type='button'
              >
                <svg
                  style={{ color: '#4caf50' }}
                  class='MuiSvgIcon-root jss413'
                  focusable='false'
                  viewBox='0 0 24 24'
                  aria-hidden='true'
                >
                  <path d='M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34a.9959.9959 0 00-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z'></path>
                </svg>
              </button>
            </Link>,
            <button
              onClick={() => handleOpen(item.id)}
              class='MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss97 jss111 jss412'
              tabindex='0'
              type='button'
            >
              <svg
                style={{ color: '#f44336' }}
                class='MuiSvgIcon-root jss413'
                focusable='false'
                viewBox='0 0 24 24'
                aria-hidden='true'
              >
                <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z'></path>
              </svg>
            </button>
            // <div>
            //   <button type='button' onClick={() => handleOpen(item.id)}>
            //     react-transition-group
            //   </button>
            // </div>
          ])
        )
        setResDatas(buffer)

        console.log(resDatas, '---buffer==')

        // return Router.push('/admin/table-list')
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch(err => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        console.log('token print----- : ', err)
      })
  }, [refreshDeleteFlag])

  const handleSearch = e => {
    console.log('e------:', e)
    let searchKeyTemp = ''
    let fromFilterTemp = ''
    let toFilterTemp = ''

    if (e.target.name === 'search_key') {
      searchKeyTemp = e.target.value
      // setSearchKey(e.target.value)
    }
    if (e.target.name === 'searchTo') {
      if (fromFilter && e.target.value) {
        fromFilterTemp = fromFilter - 1
        toFilterTemp = e.target.value - fromFilter + 1
      }
    }

    // let searchUrl = `${BASE_URL}/api/search-dealer?searchKey=${
    //   e.target.name === 'search_key' ? searchKeyTemp : searchKey
    // }&from=${fromFilterTemp}&to=${toFilterTemp}&upazila=${values.upazila}`

    // if (searchKey) {
    //   searchUrl = `${BASE_URL}/api/search-dealer/${searchKey}`
    // } else searchUrl = `${BASE_URL}/api/products`

    // if (fromFilter && toFilter) {
    //   searchUrl = `${BASE_URL}/api/search-dealer?from=${fromFilter}&to=${toFilter}`
    //   // searchUrl = `${BASE_URL}/api/search-dealer`
    // } else searchUrl = `${BASE_URL}/api/products`

    return (
      axios({
        url: `${BASE_URL}/api/search-dealer?searchById=${searchKey}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        // data: submitData,
        method: 'get'
      })
        .then(response => {
          let bufferDealer = []

          // setResDatas(response.data);
          console.log('Dealer Data print----- : ', response.data)

          response.data.map(item => {
            // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

            item.allotments.map(alltmnt => {
              bufferDealer.push([
                item?.name,
                item?.shop_name,
                alltmnt?.allotment_id,
                alltmnt?.allotment?.allotment_date,
                item?.upazila,
                item?.contract_status,
                item?.dealer_id,
                alltmnt?.is_released
              ])
            })
          })

          setDealerList(bufferDealer)

          console.log('---Dealer buffer--- :', dealerList)
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('dealer token print----- : ', err)
        })
    )
  }

  const assignDealerList = () => {
    return (
      <>{console.log('dealerList---2', dealerList)}
        {dealerList.map((iteam, i) => 
          iteam[7] == 0 ? (
          <tr>
            {/* <td>{++serial}</td> */}
            <td>{iteam[0]}</td>
            <td>{iteam[1]}</td>
            <td>{iteam[2]}</td>
            <td>{iteam[3]}</td>
            <td>
              {/* <Chip icon={<FiberManualRecordIcon />} label='PENDING' /> */}
              <Chip style={{color: '#4CAF50'}} icon={<FiberManualRecordIcon style={{color: '#4CAF50'}} />} label='VERIFIED' />
            </td>
            <td>
              <TextField
                label='OTP'
                inputProps={{ maxLength: 6 }}
                id='outlined-margin-dense'
                className={classes.textField}
                style={{ width: '110px' }}
                margin='dense'
                variant='outlined'
                onChange={e => {
                  setWarehouseOtp(e.target.value)
                }}
              />{' '}
              <Button
                color='primary'
                style={{
                  background: 'transparent',
                  // color: '#4CAF50',
                  color: '#000',
                  height: 'fit-content',
                  padding: '10px 22px',
                  marginTop: '9px',
                  border: '1px solid #D3D3D3',
                  textTransform: 'none',
                  borderRadius: '20px',
                  boxShadow: 'none'
                }}
                // onClick={() => {
                //   handleAssignDealers()
                //   handleClose()
                // }}
                // onClick={e => handleClearFilters(e)}
                onClick={() => {
                  setIsLoading(true)
                  handleOpenProductRemoveModal()
                  handleCheckOtp(i)
                  setSelectedAllotmentId(iteam[2])
                }}
              >
                Check
              </Button>
            </td>
          </tr>
        ) : '' )}
      </>
    )
  }

  const deleteDealer = id => {
    return (
      axios({
        url: `${BASE_URL}/api/delete/${id}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        // data: submitData,
        method: 'DELETE'
      })
        .then(response => {
          console.log('token print----- : ', response.data)
          // return Router.push('/admin/table-list')
          setRefreshDeleteFlag(!refreshDeleteFlag)
          handleClose()
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  return (
    <Admin>
      <GridContainer>
        {/* -- Remove Product popup -- */}
        <Modal
          aria-labelledby='transition-modal-title'
          aria-describedby='transition-modal-description'
          className={classesModal.modal}
          open={openProductRemoveModal}
          onClose={handleCloseProductRemoveModal}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500
          }}
        >
          <Fade in={openProductRemoveModal}>
            <div
              className={classesModal.paperSave}
              style={{ textAlign: 'center', width: '20vw', border: 'none' }}
            >
              <h2 id='transition-modal-title'>
                {isLoading
                  ? 'Checking...'
                  : isCorrectOtp
                  ? 'Correct OTP!'
                  : 'Wrong OTP!'}
              </h2>
              {isLoading ? (
                <CircularProgress style={{ color: '#4CAF50' }} />
              ) : isCorrectOtp ? (
                <CheckCircleIcon
                  style={{ color: '#4CAF50', fontSize: '55px' }}
                />
              ) : (
                <CancelIcon style={{ color: '#E21800', fontSize: '55px' }} />
              )}
              {/* {isLoading ? '' : <CheckCircleIcon style={{color: '#4CAF50', fontSize: '55px'}} />} */}
              <br />
              <br />

              {isLoading ? (
                ''
              ) : isCorrectOtp ? (
                <Link
                  href={`/admin/details-allotment?id=${selectedAllotmentId}&dealerId=${dealerList[0][6]}`}
                >
                  <Button
                    variant='contained'
                    color='secondary'
                    // className={classes.button}
                    style={{
                      background: 'transparent',
                      color: '#4CAF50',
                      border: '1px solid #4CAF50',
                      fontWeight: 'bold'
                    }}
                    // startIcon={<SaveIcon />}
                    // onClick={() => handleRemoveClick(deleteIndex)}
                  >
                    View allotment details
                  </Button>
                </Link>
              ) : (
                <Button
                  variant='contained'
                  color='secondary'
                  // className={classes.button}
                  style={{
                    background: 'transparent',
                    color: '#3C4858',
                    border: '1px solid #3C4858',
                    fontWeight: 'bold'
                  }}
                  // startIcon={<SaveIcon />}
                  onClick={() => handleCloseProductRemoveModal()}
                >
                  Close
                </Button>
              )}

              {/* <Button
                variant='contained'
                color='secondary'
                // className={classes.button}
                startIcon={<CancelIcon />}
                onClick={() => handleCloseProductRemoveModal()}
              >
                Cancel
              </Button>
               */}
            </div>
          </Fade>
        </Modal>

        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader
              color='primary'
              style={{
                background: '#4CAF50',
                boxShadow:
                  '0 4px 20px 0 rgb(0 0 0 / 14%), 0 7px 10px -5px rgb(80 78 80 / 40%)'
              }}
            >
              <h4 className={classes.cardTitleWhite}>Verify</h4>
              {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
            </CardHeader>
            <CardBody>
              <br />
              <br />
              <div
                style={{
                  display: 'flex',
                  // justifyContent: 'center'
                  merginTop: '20px',
                  justifyContent: 'flex-start'
                }}
              >
                <div className={classes.search} style={{ height: '56px' }}>
                  <div className={classes.searchIcon}>
                    <SearchIcon />
                  </div>
                  <InputBase
                    placeholder='Enter dealer id'
                    classes={{
                      root: classes.inputRoot,
                      input: classes.inputInput
                    }}
                    value={searchKey}
                    inputProps={{ 'aria-label': 'search' }}
                    name='search_key'
                    onChange={e => {
                      setSearchKey(e.target.value)
                      // handleSearch(e)
                    }}
                    style={{ marginTop: '1px' }}
                  />
                </div>

                <Button
                  color='primary'
                  style={{ background: '#4CAF50', margin: '2px' }}
                  // onClick={() => {
                  //   handleAssignDealers()
                  //   handleClose()
                  // }}
                  onClick={e => handleSearch(e)}
                >
                  Search
                </Button>
                {/* <InputLabel id='demo-mutiple-checkbox-label'>
                          Tag
                        </InputLabel>
                        <Select
                          labelId='demo-mutiple-checkbox-label'
                          id='demo-mutiple-checkbox'
                          multiple
                          value={personName}
                          onChange={event => setPersonName(event.target.value)}
                          input={<Input />}
                          // renderValue={(selected) => selected.join(', ')}
                          renderValue={selected => selected.length}
                          MenuProps={MenuProps}
                        >
                          {names.map(name => (
                            <MenuItem key={name} value={name}>
                              <Checkbox
                                checked={personName.indexOf(name) > -1}
                              />
                              <ListItemText primary={name} />
                            </MenuItem>
                          ))}
                        </Select> */}

                {/* <InputLabel
                        style={{ marginBottom: '10px' }}
                        htmlFor='outlined-age-simple'
                      >
                        Don't include
                      </InputLabel>
                      <Select
                        // value={values.distanceZonal}
                        // onChange={e => setDistanceZonal(e.target.value)}
                        onChange={handleInputValue}
                        onBlur={handleInputValue}
                        // name='distanceZonal'
                        multiple
                        value={personName}
                        input={
                          <OutlinedInput
                            name='dontInclude'
                            id='outlined-age-simple'
                          />
                        }
                      >
                        <MenuItem value={'recentAllotedDealers'}>
                          <Checkbox
                            // checked={state.item.dealer_id}
                            checked={checkedRecentAlloted}
                            onChange={handleDontInclude}
                            name='recentAllotedDealers'
                            inputProps={{
                              'aria-label': 'primary checkbox'
                            }}
                            style={{ color: '#4CAF50' }}
                          />
                          Recently allotted dealers
                        </MenuItem>
                        <MenuItem value={'contractExpiry'}>
                          <Checkbox
                            // checked={state.item.dealer_id}
                            checked={checkedContractExpiry}
                            onChange={handleDontInclude}
                            name='contractExpiry'
                            inputProps={{
                              'aria-label': 'primary checkbox'
                            }}
                            style={{ color: '#4CAF50' }}
                          />
                          Contract expires in 1 month
                        </MenuItem>
                        <MenuItem value={'sameFamilyMember'}>
                          <Checkbox
                            // checked={state.item.dealer_id}
                            checked={checkedFamilyMember}
                            onChange={handleDontInclude}
                            name='sameFamilyMember'
                            inputProps={{
                              'aria-label': 'primary checkbox'
                            }}
                            style={{ color: '#4CAF50' }}
                          />
                          Same family members
                        </MenuItem>
                      </Select> */}
              </div>
              <br />
              <br />
              <div className={'assign_dealers'} style={{ overflowX: 'auto' }}>
                <table>
                  <tr>
                    {/* <th>Select Dealer</th> */}
                    {/* <th>Serial</th> */}
                    <th>Dealer Name</th>
                    <th>Shop name</th>
                    <th>Allotment number</th>
                    <th>Allotment date</th>
                    <th>Payment</th>
                    <th>Verify</th>
                  </tr>
                  {assignDealerList()}
                </table>
              </div>
            </CardBody>
            <CardFooter>
              {/* <Link href='/admin/add-new-dealer'> */}
              {/* <Button
                        color='primary'
                        style={{ background: '#4CAF50' }}
                        onClick={() => {
                          handleAssignDealers()
                          handleClose()
                        }}
                      >
                        Assign
                      </Button>
                      <Button
                        color='primary'
                        style={{ background: '#4CAF50' }}
                        onClick={() => handleClose()}
                      >
                        Cancel
                      </Button> */}
              {/* </Link> */}
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    </Admin>
  )
}

// Dealers.layout = Admin

export default WithAuth(Dealers)
