import React, { useState } from 'react'
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles'
import InputLabel from '@material-ui/core/InputLabel'
// layout for this page
import Admin from 'layouts/Admin.js'
// core components
import GridItem from 'components/Grid/GridItem.js'
import GridContainer from 'components/Grid/GridContainer.js'
import CustomInput from 'components/CustomInput/CustomInput.js'
import Button from 'components/CustomButtons/Button.js'
import Card from 'components/Card/Card.js'
import CardHeader from 'components/Card/CardHeader.js'
import CardAvatar from 'components/Card/CardAvatar.js'
import CardBody from 'components/Card/CardBody.js'
import CardFooter from 'components/Card/CardFooter.js'

import avatar from 'assets/img/faces/marc.jpg'
import tcb_logo from 'assets/img/tcb_logo.png'

import TextField from '@material-ui/core/TextField'
import axios from 'axios'
import Router, { withRouter } from 'next/router'
import { BASE_URL } from '../../env.js'
import Cookies from 'js-cookie'
import FormControl from '@material-ui/core/FormControl'
import IconButton from '@material-ui/core/IconButton'
import Input from '@material-ui/core/Input'
import FilledInput from '@material-ui/core/FilledInput'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import InputAdornment from '@material-ui/core/InputAdornment'
import FormHelperText from '@material-ui/core/FormHelperText'
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'

const styles = {
  cardCategoryWhite: {
    color: 'rgba(255,255,255,.62)',
    margin: '0',
    fontSize: '14px',
    marginTop: '0',
    marginBottom: '0'
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none'
  }
}

function UserProfile () {
  const [email, setEmail] = useState()
  const [password, setPassword] = useState()

  const [values, setValues] = React.useState({
    amount: '',
    password: '',
    weight: '',
    weightRange: '',
    showPassword: false
  })
  const handleChange = prop => event => {
    setValues({ ...values, [prop]: event.target.value })
  }

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword })
  }

  const handleMouseDownPassword = event => {
    event.preventDefault()
  }

  const submitLogin = (e) => {
    e.preventDefault()

    const submitData = {
      email: email,
      password: password
    }

    return (
      axios({
        url: `${BASE_URL}/api/login`,
        headers: {
          'Content-Type': 'application/json'
        },
        data: submitData,
        method: 'post'
      })
        .then(response => {
          console.log('token print----- : ', response.data)
          // localStorage.setItem('token', response.data.token);
          Cookies.set('cToken', response.data.token)
          return Router.push('/admin/dealers')
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )

    // try {
    //   const response = await axios.post(
    //     `${API_HOST}users/track`,
    //     { email, password },
    //     {
    //       headers: {
    //         Authorization: getToken(),
    //         'Content-Type': 'application/json',
    //         'X-OS-Version': X_OS_VERSION,
    //         'X-Device-ID': PUBLIC_AUTH_TOKEN,
    //       },
    //     }
    //   )
    //   return { success: true, data: response.data || [] }
    // } catch (err) {
    //   if (err && err.response && err.response.status === 401) {
    //     logOut()
    //   } else {
    //     return { success: false, message: err.message }
    //   }
    // }
  }

  const useStyles = makeStyles(styles)
  const classes = useStyles()
  return (
    <div>
      <GridContainer justify='center'>
        {/* <GridItem xs={12} sm={12} md={8}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Edit Profile</h4>
              <p className={classes.cardCategoryWhite}>Complete your profile</p>
            </CardHeader>
            <CardBody>
              <GridContainer>
                <GridItem xs={12} sm={12} md={5}>
                  <CustomInput
                    labelText="Company (disabled)"
                    id="company-disabled"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      disabled: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={3}>
                  <CustomInput
                    labelText="Username"
                    id="username"
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                    labelText="Email address"
                    id="email-address"
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                    labelText="First Name"
                    id="first-name"
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <CustomInput
                    labelText="Last Name"
                    id="last-name"
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                    labelText="City"
                    id="city"
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                    labelText="Country"
                    id="country"
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                    labelText="Postal Code"
                    id="postal-code"
                    formControlProps={{
                      fullWidth: true,
                    }}
                  />
                </GridItem>
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <InputLabel style={{ color: "#AAAAAA" }}>About me</InputLabel>
                  <CustomInput
                    labelText="Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo."
                    id="about-me"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      multiline: true,
                      rows: 5,
                    }}
                  />
                </GridItem>
              </GridContainer>
            </CardBody>
            <CardFooter>
              <Button color="primary">Update Profile</Button>
            </CardFooter>
          </Card>
        </GridItem> */}
        <GridItem xs={12} sm={12} md={4}>
          <Card profile style={{ marginTop: '140px' }}>
            <CardAvatar profile>
              <a href='#pablo' onClick={e => e.preventDefault()}>
                <img src={tcb_logo} alt='TCB logo' />
              </a>
            </CardAvatar>
            <CardBody profile>
              {/* <h6 className={classes.cardCategory}>CEO / CO-FOUNDER</h6> */}
              <h4
                style={{ fontWeight: 'bold', marginBottom: 0 }}
                className={classes.cardTitle}
              >
                Trading Corporation of Bangladesh
              </h4>
              <p className={classes.description}>Truck Sell Digitalization</p>{' '}
              <br />
              <form onSubmit={submitLogin}>
                <TextField
                  style={{ width: '239px' }}
                  id='outlined-basic'
                  label='Email'
                  variant='outlined'
                  onChange={e => setEmail(e.target.value)}
                />{' '}
                <br /> <br />
                {/* <TextField
                id='outlined-basic'
                type='password'
                label='Password'
                variant='outlined'
                onChange={e => setPassword(e.target.value)}
              /> */}
                <FormControl
                  // className={clsx(classes.margin, classes.textField)}
                  variant='outlined'
                >
                  <InputLabel htmlFor='outlined-adornment-password'>
                    Password
                  </InputLabel>
                  <OutlinedInput
                    id='outlined-adornment-password'
                    type={values.showPassword ? 'text' : 'password'}
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    endAdornment={
                      <InputAdornment position='end'>
                        <IconButton
                          aria-label='toggle password visibility'
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge='end'
                        >
                          {values.showPassword ? (
                            <Visibility />
                          ) : (
                            <VisibilityOff />
                          )}
                        </IconButton>
                      </InputAdornment>
                    }
                    labelWidth={70}
                  />
                </FormControl>
                <br /> <br />
                <Button
                  type='submit'
                  color='primary'
                  round
                  style={{ background: '#1BA64F' }}
                  // onClick={() => submitLogin()}
                >
                  Log In
                </Button>
              </form>
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  )
}

// UserProfile.layout = Admin;

export default UserProfile
