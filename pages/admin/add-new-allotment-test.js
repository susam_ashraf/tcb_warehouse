import React, { useState } from 'react'
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles'
import InputLabel from '@material-ui/core/InputLabel'
// layout for this page
import Admin from 'layouts/Admin.js'
// core components
import GridItem from 'components/Grid/GridItem.js'
import GridContainer from 'components/Grid/GridContainer.js'
import CustomInput from 'components/CustomInput/CustomInput.js'
import Button from 'components/CustomButtons/Button.js'
import Card from 'components/Card/Card.js'
import CardHeader from 'components/Card/CardHeader.js'
import CardAvatar from 'components/Card/CardAvatar.js'
import CardBody from 'components/Card/CardBody.js'
import CardFooter from 'components/Card/CardFooter.js'

import avatar from 'assets/img/faces/marc.jpg'

import TextField from '@material-ui/core/TextField'
import Select from '@material-ui/core/Select'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import MenuItem from '@material-ui/core/MenuItem'
import axios from 'axios'
import Router, { withRouter } from 'next/router'
import { useRouter } from 'next/router'
import Input from '@material-ui/core/Input'
import { BASE_URL } from '../../env.js'
import Cookies from 'js-cookie'

const styles = {
  cardCategoryWhite: {
    color: 'rgba(255,255,255,.62)',
    margin: '0',
    fontSize: '14px',
    marginTop: '0',
    marginBottom: '0'
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none'
  }
}

function AddNewAllotment () {
  const useStyles = makeStyles(styles)
  const classes = useStyles()

  const { query } = useRouter()

  const [name, setName] = useState()
  const [shopName, setShopName] = useState()
  const [mobileNo, setMobileNo] = useState()
  const [zone, setZone] = useState()
  const [district, setDistrict] = useState()
  const [upazila, setUpazila] = useState()
  const [distanceZonal, setDistanceZonal] = useState()
  const [shopAddress, setShopAddress] = useState()
  // const [shopName, setShopName] = useState();
  // const [shopName, setShopName] = useState();
  // const [shopName, setShopName] = useState();

  const storeDealer = () => {
    const submitData = {
      name: name,
      shop_name: shopName,
      distance_from_zonal: distanceZonal,
      shop_address: shopAddress,
      mobile_no: mobileNo,
      zone: zone,
      district: district,
      upazila: upazila,
      contract_status: 'Permanent'
    }

    return (
      axios({
        url: `${BASE_URL}/api/create`,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${Cookies.get('cToken')}`,
      },
        data: submitData,
        method: 'post'
      })
        .then(response => {
          console.log('token print----- : ', response.data)
          return Router.push('/admin/dealers')
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  const [inputList, setInputList] = useState([{ productName: '', price: '' }])
  const [inputEdit, setInputEdit] = useState('ffffff')


    const handleEditClick = (name) => {
      setInputEdit(name)
    }

  // handle input change
  const handleInputChange = (e, index) => {
    const { name, value } = e.target
    const list = [...inputList]
    list[index][name] = value
    setInputList(list)
  }

  // handle click event of the Remove button
  const handleRemoveClick = index => {
    const list = [...inputList]
    list.splice(index, 1)
    setInputList(list)
  }

  // handle click event of the Add button
  const handleAddClick = () => {
    setInputList([...inputList, { productName: '', price: '' }])
  }

  return (
    <div className='App'>
      <h3>
        <a href='https://cluemediator.com'>Add Allotment</a>
      </h3>

      {/* {inputList.map((x, i) => {
        return (
          <div className='box'>
            <p>Product Name : {x.productName}</p>
            <p>Price : {x.price}</p>
            <br />
            <hr />
            <br />
          </div>
        )
      })} */}

      {inputList.map((x, i) => {
        return (
          <div className='box'>
            

            {inputList.length - 1 === i ? (
              <>
                <input
                  name='productName'
                  placeholder='Product Name'
                  value={x.productName}
                  onChange={e => handleInputChange(e, i)}
                />
                <input
                  className='ml10'
                  name='price'
                  placeholder='Price'
                  value={x.price}
                  onChange={e => handleInputChange(e, i)}
                />
                <div className='btn-box'>
                  {inputList.length !== 1 && (
                    <button
                      className='mr10'
                      onClick={() => handleRemoveClick(i)}
                    >
                      Remove
                    </button>
                  )}
                  {inputList.length - 1 === i && (
                    <button onClick={handleAddClick}>Add</button>
                  )}
                </div>
                <br />
              </>
            ) : (
              <>{x.productName && (
              <>
                <p>Product Name : {x.productName}</p>
                <p>Price : {x.price}</p>
                
                {/* <button onClick={handleEditClick(x.productName)}>Edit</button> */}
                <br />
              </>
            )}
            </>)
            }
          </div>
        )
      })}

      <hr />
      <br />

      {inputList.map((x, i) => {
        return (
          <div className='box'>
            <input
              name='productName'
              placeholder='Product Name'
              value={x.productName}
              onChange={e => handleInputChange(e, i)}
            />
            <input
              className='ml10'
              name='price'
              placeholder='Price'
              value={x.price}
              onChange={e => handleInputChange(e, i)}
            />
            <div className='btn-box'>
              {inputList.length !== 1 && (
                <button className='mr10' onClick={() => handleRemoveClick(i)}>
                  Remove
                </button>
              )}
              {inputList.length - 1 === i && (
                <button onClick={handleAddClick}>Add</button>
              )}
            </div>
            <br />
          </div>
        )
      })}
      <div style={{ marginTop: 20 }}>{JSON.stringify(inputList)}</div>
    </div>
  )
}

AddNewAllotment.layout = Admin

export default AddNewAllotment
