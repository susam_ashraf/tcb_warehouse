import React from 'react'
// @material-ui/core components
import { makeStyles, useTheme } from '@material-ui/core/styles'
// layout for this page
import Admin from 'layouts/Admin.js'
// core components
import GridItem from 'components/Grid/GridItem.js'
import GridContainer from 'components/Grid/GridContainer.js'
import Table from 'components/Table/Table.js'
import Card from 'components/Card/Card.js'
import CardHeader from 'components/Card/CardHeader.js'
import CardBody from 'components/Card/CardBody.js'
import CardFooter from 'components/Card/CardFooter.js'
import Button from 'components/CustomButtons/Button.js'
import Link from 'next/link'
import axios from 'axios'
import { useEffect, useState } from 'react'
import Router, { withRouter, useRouter } from 'next/router'
import Select from '@material-ui/core/Select'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import MenuItem from '@material-ui/core/MenuItem'
import TextField from '@material-ui/core/TextField'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import { BASE_URL } from '../../env.js'
import Cookies from 'js-cookie'
import { districtList, upazilaList } from './constData.json'
import stylesCustom from './add-new-allotment.module.css'
import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import SaveIcon from '@material-ui/icons/Save'
import CancelIcon from '@material-ui/icons/Cancel'
import SendIcon from '@material-ui/icons/Send'

import Checkbox from '@material-ui/core/Checkbox'
import InputBase from '@material-ui/core/InputBase'
import SearchIcon from '@material-ui/icons/Search'
import { useFormControls } from '../../components/allotments/allotmentValidation'
import ListItemText from '@material-ui/core/ListItemText'

// const styles = {
//   cardCategoryWhite: {
//     '&,& a,& a:hover,& a:focus': {
//       color: 'rgba(255,255,255,.62)',
//       margin: '0',
//       fontSize: '14px',
//       marginTop: '0',
//       marginBottom: '0'
//     },
//     '& a,& a:hover,& a:focus': {
//       color: '#FFFFFF'
//     }
//   },
//   cardTitleWhite: {
//     color: '#FFFFFF',
//     marginTop: '0px',
//     minHeight: 'auto',
//     fontWeight: '300',
//     fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
//     marginBottom: '3px',
//     textDecoration: 'none',
//     '& small': {
//       color: '#777',
//       fontSize: '65%',
//       fontWeight: '400',
//       lineHeight: '1'
//     }
//   },
//   search: {
//     position: 'relative',
//     borderRadius: theme.shape.borderRadius,
//     backgroundColor: alpha(theme.palette.common.white, 0.15),
//     '&:hover': {
//       backgroundColor: alpha(theme.palette.common.white, 0.25),
//     },
//     marginRight: theme.spacing(2),
//     marginLeft: 0,
//     width: '100%',
//     [theme.breakpoints.up('sm')]: {
//       marginLeft: theme.spacing(3),
//       width: 'auto',
//     },
//   },
//   searchIcon: {
//     padding: theme.spacing(0, 2),
//     height: '100%',
//     position: 'absolute',
//     pointerEvents: 'none',
//     display: 'flex',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   inputRoot: {
//     color: 'inherit',
//   },
//   inputInput: {
//     padding: theme.spacing(1, 1, 1, 0),
//     // vertical padding + font size from searchIcon
//     paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
//     transition: theme.transitions.create('width'),
//     width: '100%',
//     [theme.breakpoints.up('md')]: {
//       width: '20ch',
//     },
//   },
// }

const useStylesModal = makeStyles(theme => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    width: '85vw',
    height: '90vh',
    overflow: 'scroll'
  },
  paperSave: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  }
}))

function DetailsAllotment () {
  const classesModal = useStylesModal()
  const [open, setOpen] = React.useState(false)
  const [openSaveModal, setOpenSaveModal] = React.useState(false)
  const [openProductRemoveModal, setOpenProductRemoveModal] = React.useState(
    false
  )
  const [deleteIndex, setDeleteIndex] = React.useState(99)

  const {
    handleInputValue,
    handleFormSubmit,
    formIsValid,
    errors,
    values
  } = useFormControls()

  const handleOpen = id => {
    setOpen(true)
  }
  const handleClose = () => {
    setSelectedDealerIds([])
    setOpen(false)
  }

  const handleOpenSaveModal = () => {
    setOpenSaveModal(true)
  }

  const handleCloseSaveModal = () => {
    setOpenSaveModal(false)
  }

  const handleOpenProductRemoveModal = index => {
    setOpenProductRemoveModal(true)
    setDeleteIndex(index)
  }

  const handleCloseProductRemoveModal = () => {
    setOpenProductRemoveModal(false)
  }

  const useStyles = makeStyles(theme => ({
    cardCategoryWhite: {
      '&,& a,& a:hover,& a:focus': {
        color: 'rgba(255,255,255,.62)',
        margin: '0',
        fontSize: '14px',
        marginTop: '0',
        marginBottom: '0'
      },
      '& a,& a:hover,& a:focus': {
        color: '#FFFFFF'
      }
    },
    cardTitleWhite: {
      color: '#FFFFFF',
      marginTop: '0px',
      minHeight: 'auto',
      fontWeight: '300',
      fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
      marginBottom: '3px',
      textDecoration: 'none',
      '& small': {
        color: '#777',
        fontSize: '65%',
        fontWeight: '400',
        lineHeight: '1'
      }
    },
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      // backgroundColor: alpha(theme.palette.common.white, 0.15),
      // '&:hover': {
      //   backgroundColor: alpha(theme.palette.common.white, 0.25),
      // },
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto'
      },
      border: '1px solid #D3D3D3'
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    inputRoot: {
      color: 'inherit'
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '20ch'
      },
      marginTop: '8px'
    }
  }))
  const classes = useStyles()

  const [resDatas, setResDatas] = useState([])
  const [dealerList, setDealerList] = useState([])
  const [allotmentDealerList, setAllotmentDealerList] = useState([])
  const [allotmentDealer, setAllotmentDealer] = useState()
  const [isReleased, setIsReleased] = useState(1)
  const [recentAllotedDealerList, setRecentAllotedDealerList] = useState([])
  const [excludeDateDealerList, setExcludeDateDealerList] = useState([])
  const [excludeDate, setExcludeDate] = useState('')
  const [allotmentId, setAllotmentId] = useState()
  const [contractExpiryList, setContractExpiryList] = useState([])
  const [dealerIdList, setDealerIdList] = useState(['013701011', '013701015'])
  const [refreshDeleteFlag, setRefreshDeleteFlag] = useState(true)
  const [zone, setZone] = useState('Dhaka')
  const [district, setDistrict] = useState()
  const [upazila, setUpazila] = useState()
  const [distanceZonal, setDistanceZonal] = useState()
  const [allotmentDate, setAllotmentDate] = useState()
  const [fiscalYear, setFiscalYear] = useState()
  const [fromFilter, setFromFilter] = useState('')
  const [toFilter, setToFilter] = useState('')
  const [searchKey, setSearchKey] = useState('')
  const [clearSelected, setClearSelected] = useState(['1', '2'])

  const [todos, setTodos] = useState(['013701011', '013701015'])

  const [subTotal, setSubTotal] = useState(0)

  const [inputList, setInputList] = useState([
    { product_name: '', amount: '', unit: '', price: '', transport_cost: '' }
  ])

  const [selectedDate, setSelectedDate] = React.useState(
    new Date('2014-08-18T21:11:54')
  )

  // for checkbox----

  const [checked, setChecked] = React.useState(false)
  const [checkedRecentAlloted, setCheckedRecentAlloted] = useState(false)
  const [checkedContractExpiry, setCheckedContractExpiry] = useState(false)
  const [checkedFamilyMember, setCheckedFamilyMember] = useState(false)
  const [selectedDealerIds, setSelectedDealerIds] = useState([])
  const [mainSelectedDealerIds, setMainSelectedDealerIds] = useState([])
  const [removeDealerIds, setRemoveDealerIds] = useState([])
  const [personName, setPersonName] = React.useState([])
  const [state, setState] = React.useState({
    checkedA: false,
    checkedB: false,
    checkedC: false,
    checkedG: false
  })

  const names = [
    'Oliver Hansen',
    'Van Henry',
    'April Tucker',
    'Ralph Hubbard',
    'Omar Alexander',
    'Carlos Abbott',
    'Miriam Wagner',
    'Bradley Wilkerson',
    'Virginia Andrews',
    'Kelly Snyder'
  ]
  const ITEM_HEIGHT = 48
  const ITEM_PADDING_TOP = 8
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250
      }
    }
  }
  // const handleChange = event => {
  //   setChecked(event.target.checked)
  // }

  const handleAdd = () => {
    const newTodos = [...todos]
    newTodos.push('ashraf')
    setTodos(newTodos)
    console.log('newTodos--', todos)
  }

  const handleDontInclude = event => {
    if (event.target.name === 'recentAllotedDealers') {
      setCheckedRecentAlloted(event.target.checked)
    }
    if (event.target.name === 'contractExpiry') {
      setCheckedContractExpiry(event.target.checked)
    }
    if (event.target.name === 'sameFamilyMember') {
      setCheckedFamilyMember(event.target.checked)
    }
  }

  const handleChange = event => {
    // const selectedId = parseInt(event.target.name);
    const selectedId = event.target.name
    console.log('event-----', event.target.name)

    // let newIds = [...dealerIdList]
    // console.log('newIds--', dealerIdList)
    // console.log('newIds--', newIds)
    // newIds.push(event.target.name)
    // console.log('newIds--2', newIds)
    // setDealerIdList(newIds)

    // Check if "ids" contains "selectedIds"
    // If true, this checkbox is already checked
    // Otherwise, it is not selected yet

    if (selectedDealerIds.includes(selectedId)) {
      console.log('ids include true')
      const newIds = selectedDealerIds.filter(id => id !== selectedId)
      setSelectedDealerIds(newIds)
      console.log('newIds--2', selectedDealerIds)
    } else {
      console.log('ids include False')
      const newIds = [...selectedDealerIds]
      console.log('newIds--', newIds)
      newIds.push(selectedId)
      console.log('newIds--2', newIds)
      setSelectedDealerIds(newIds)
    }
  }

  const handleAssignDealers = event => {
    // const mainIds = [...selectedDealerIds]
    const mainIds = [...mainSelectedDealerIds, ...selectedDealerIds]
    setMainSelectedDealerIds(mainIds)
  }

  const handleRemoveChange = event => {
    const removedId = event.target.name

    if (removeDealerIds.includes(removedId)) {
      const newIds = removeDealerIds.filter(id => id !== removedId)
      setRemoveDealerIds(newIds)
    } else {
      const newIds = [...removeDealerIds]
      newIds.push(removedId)
      setRemoveDealerIds(newIds)
    }
  }
  const handleRemoveDealers = () => {
    let mainIds = [...mainSelectedDealerIds]
    removeDealerIds.map(item => (mainIds = mainIds.filter(id => id !== item)))
    setMainSelectedDealerIds(mainIds)
    setRemoveDealerIds([])
  }
  const handleCancelRemove = () => {
    setRemoveDealerIds([])
  }

  // end for checkbox----

  const handleDateChange = date => {
    setSelectedDate(date)
  }

  const showSubTotal = total => {
    // setSubTotal(subTotal + total)
    return total
  }

  let buffer = []
  let bufferDealer = []

  useEffect(() => {
    let calcSub = 0

    inputList.map((x, i) => {
      if (x.amount && x.price && x.transport_cost) {
        calcSub = calcSub + +(x.amount * x.price) + +x.transport_cost
        setSubTotal(calcSub)
      }
    })

    // {x.amount && x.price && x.transport_cost
    //   ? showSubTotal(+(x.amount * x.price) + +x.transport_cost)
    //   : ''}
  }, [inputList])
  const { query } = useRouter()
  useEffect(() => {
    let productData = axios({
      url: `${BASE_URL}/api/warehouse/allotment-details?allotmentId=${query.id}&dealerId=${query.dealerId}`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${Cookies.get('cToken')}`
      },
      // data: submitData,
      method: 'get'
    })
      .then(response => {
        console.log('Single from use effect print----- : ', response.data)

        let data = response.data

        // setDealerSingle({
        //   name: response.data.name,
        //   mobile_no: response.data.mobile_no,
        //   district: response.data.district,
        //   distance_from_zonal: response.data.distance_from_zonal,
        //   contract_status: response.data.contract_status,
        //   shop_address: response.data.shop_address,
        //   shop_name: response.data.shop_name,
        //   upazila: response.data.upazila,
        //   zone: response.data.zone
        // });

        setAllotmentDate(data.allotment.allotment_date)
        setInputList(data.allotment.allotted_products)
        setAllotmentId(data.allotment_id)
        setFiscalYear(data.allotment.fiscal_year)
        setZone(data.allotment.zone)
        setDistrict(data.allotment.district)
        setUpazila(data.allotment.upazila)
        setAllotmentDealer(data.dealer)
        setIsReleased(data.is_released)

        // setDistanceZonal(data.distance_from_zonal);
        // setShopAddress(data.shop_address);
        // setContractStatus(data.contract_status);

        data.allotment.allotted_products.map(item =>
          // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

          bufferDealer.push([
            item.product.dealer_id,
            item.product.name,
            item.product.mobile_no,
            item.product.shop_name,
            item.product.upazila,
            item.product.contract_status,
            item.product.contract_expiry_date,
            item.product.family_dealer_id
          ])
        )

        setDealerList(bufferDealer)

        return response.data
        // return Router.push('/admin/table-list')
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch(err => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        console.log('token print----- : ', err)
      })
  }, [])

  const assignDealersNoFilter = () => {
    return (
      axios({
        url: `${BASE_URL}/api/products?upazila=${values.upazila}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        // data: submitData,
        method: 'get'
      })
        .then(response => {
          // setResDatas(response.data);
          console.log('Dealer Data print----- : ', response.data)

          response.data.map(item =>
            // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

            bufferDealer.push([
              item.dealer_id,
              item.name,
              item.mobile_no,
              item.shop_name,
              item.upazila,
              item.contract_status,
              item.contract_expiry_date,
              item.family_dealer_id
            ])
          )

          setDealerList(bufferDealer)

          console.log('---Dealer buffer--- :', dealerList)
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('dealer token print----- : ', err)
        })
    )
  }

  const releaseAllotment = () => {
    const submitData = {
      is_released: 1
    }

    return (
      axios({
        url: `${BASE_URL}/api/warehouse/release-allotment?dealerId=${query.dealerId}&allotmentId=${query.id}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        data: submitData,
        method: 'put'
      })
        .then(response => {
          console.log('token print----- : ', response.data)
          return Router.push('/admin/allotments')
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  useEffect(() => {
    let bufferDontInclude = []
    let bufferContractExpire = []
    let today = new Date()
    axios({
      url: `${BASE_URL}/api/allotments/recently-alloted-dealers`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${Cookies.get('cToken')}`
      },
      // data: submitData,
      method: 'get'
    })
      .then(response => {
        // setResDatas(response.data);
        console.log('Dealer Data print----- : ', response.data)

        response.data.map(item => {
          // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

          bufferDontInclude.push(item.product_id)

          const Difference_In_Days =
            (new Date(item.product.contract_expiry_date).getTime() -
              today.getTime()) /
            (1000 * 3600 * 24)

          console.log('Difference_In_Days----:', Difference_In_Days)

          if (Difference_In_Days < 30) {
            bufferContractExpire.push(item.product_id)
          }
        })

        setRecentAllotedDealerList(bufferDontInclude)

        setContractExpiryList(bufferContractExpire)

        console.log('---Recent Alloted buffer--- :', recentAllotedDealerList)
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch(err => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        console.log('Recent Alloted print----- : ', err)
      })
  }, [])

  // useEffect(() => {
  //   let initialData = axios({
  //     url: `${BASE_URL}/api/products?upazila=${values.upazila}`,
  //     headers: {
  //       'Content-Type': 'application/json',
  //       Authorization: `Bearer ${Cookies.get('cToken')}`
  //     },
  //     // data: submitData,
  //     method: 'get'
  //   })
  //     .then(response => {
  //       // setResDatas(response.data);
  //       console.log('Dealer Data print----- : ', response.data)

  //       response.data.map(item =>
  //         // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

  //         bufferDealer.push([
  //           item.dealer_id,
  //           item.name,
  //           item.mobile_no,
  //           item.shop_name,
  //           item.upazila,
  //           item.contract_status,
  //           item.contract_expiry_date,
  //           item.family_dealer_id
  //         ])
  //       )

  //       setDealerList(bufferDealer)

  //       console.log('---Dealer buffer--- :', dealerList)
  //     })
  //     // .then((json) => ({
  //     //   type: 'SUCCESS',
  //     //   payload: json,
  //     // }))
  //     .catch(err => {
  //       // if (getToken() && err && err.response && err.response.status === 401) {
  //       //   logOut()
  //       // } else {
  //       //   return {
  //       //     type: 'FAIL',
  //       //   }
  //       // }
  //       console.log('dealer token print----- : ', err)
  //     })
  // }, [values.upazila])

  useEffect(() => {
    inputList.map((x, i) =>
      // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

      buffer.push([
        <Select
          fullWidth
          // value={{
          //   age: '',
          //   name: 'hai',
          //   labelWidth: 0,
          // }}
          value={x.product_name}
          onChange={e => handleInputChange(e, i)}
          input={
            <OutlinedInput
              // labelWidth={100}
              name='product_name'
              id='outlined-age-simple'
            />
          }
          disabled
        >
          <MenuItem value={'sugar'}>Sugar</MenuItem>
          <MenuItem value={'rice'}>Rice</MenuItem>
          <MenuItem value={'onion'}>Onion</MenuItem>
          <MenuItem value={'soyabean2'}>soyabean oil - 2 litre</MenuItem>
          <MenuItem value={'soyabean5'}>soyabean oil - 5 litre</MenuItem>
          <MenuItem value={'Potato'}>Potato</MenuItem>
        </Select>,
        <div style={{ display: 'flex', maxWidth: '250px' }}>
          <TextField
            id='standard-basic'
            label=''
            type='number'
            name='amount'
            value={x.amount}
            variant='outlined'
            onChange={e => handleInputChange(e, i)}
            style={{ marginRight: '10px' }}
            disabled
          />

          <Select
            fullWidth
            // value={{
            //   age: '',
            //   name: 'hai',
            //   labelWidth: 0,
            // }}
            value={x.unit}
            onChange={e => handleInputChange(e, i)}
            input={
              <OutlinedInput
                // labelWidth={100}
                name='unit'
                id='outlined-age-simple'
              />
            }
            disabled
          >
            <MenuItem value={'kg'}>Kg</MenuItem>
            <MenuItem value={'litre'}>Litre</MenuItem>
          </Select>
        </div>,
        <TextField
          id='standard-basic'
          label=''
          name='price'
          value={x.price}
          variant='outlined'
          onChange={e => handleInputChange(e, i)}
          disabled
        />,
        <TextField
          id='standard-basic'
          label=''
          name='transport_cost'
          value={x.transport_cost}
          variant='outlined'
          onChange={e => handleInputChange(e, i)}
          disabled
        />,
        <>
          {x.amount && x.price && x.transport_cost
            ? showSubTotal(+(x.amount * x.price) + +x.transport_cost)
            : ''}
          {}
        </>
        // <button
        //   // onClick={() => handleOpen(item.id)}
        //   // onClick={() => handleRemoveClick(i)}
        //   onClick={() => handleOpenProductRemoveModal(i)}
        //   class='MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss97 jss111 jss412'
        //   tabindex='0'
        //   type='button'
        //   disabled
        // >
        //   <svg
        //     style={{ color: '#f44336' }}
        //     class='MuiSvgIcon-root jss413'
        //     focusable='false'
        //     viewBox='0 0 24 24'
        //     aria-hidden='true'
        //   >
        //     <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z'></path>
        //   </svg>
        // </button>
      ])
    )

    // buffer.push([
    //   "", "", "", "Subtotal : ", subTotal
    // ])
    setResDatas(buffer)
  }, [inputList])

  const deleteDealer = id => {
    return (
      axios({
        url: `${BASE_URL}/api/delete/${id}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        // data: submitData,
        method: 'DELETE'
      })
        .then(response => {
          console.log('token print----- : ', response.data)
          // return Router.push('/admin/table-list')
          setRefreshDeleteFlag(!refreshDeleteFlag)
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  // handle input change
  const handleInputChange = (e, index) => {
    const { name, value } = e.target
    const list = [...inputList]
    list[index][name] = value
    setInputList(list)
  }

  // handle click event of the Remove button
  const handleRemoveClick = index => {
    const list = [...inputList]
    list.splice(index, 1)
    setInputList(list)
    handleCloseProductRemoveModal()
  }

  // handle click event of the Add button
  const handleAddClick = () => {
    setInputList([
      ...inputList,
      { product_name: '', amount: '', unit: '', price: '', transport_cost: '' }
    ])
  }
  const handleSearch = e => {
    console.log('e------:', e)
    let searchKeyTemp = ''
    let fromFilterTemp = ''
    let toFilterTemp = ''

    if (e.target.name === 'search_key') {
      searchKeyTemp = e.target.value
      // setSearchKey(e.target.value)
    }
    if (e.target.name === 'searchTo') {
      if (fromFilter && e.target.value) {
        fromFilterTemp = fromFilter - 1
        toFilterTemp = e.target.value - fromFilter + 1
      }
    }

    let searchUrl = `${BASE_URL}/api/search-dealer?searchKey=${
      e.target.name === 'search_key' ? searchKeyTemp : searchKey
    }&from=${fromFilterTemp}&to=${toFilterTemp}&upazila=${values.upazila}`
    // if (searchKey) {
    //   searchUrl = `${BASE_URL}/api/search-dealer/${searchKey}`
    // } else searchUrl = `${BASE_URL}/api/products`

    // if (fromFilter && toFilter) {
    //   searchUrl = `${BASE_URL}/api/search-dealer?from=${fromFilter}&to=${toFilter}`
    //   // searchUrl = `${BASE_URL}/api/search-dealer`
    // } else searchUrl = `${BASE_URL}/api/products`

    return (
      axios({
        url: searchUrl,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        // data: submitData,
        method: 'get'
      })
        .then(response => {
          // setResDatas(response.data);
          console.log('Dealer Data print----- : ', response.data)

          response.data.map(item =>
            // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

            bufferDealer.push([
              item.dealer_id,
              item.name,
              item.mobile_no,
              item.shop_name,
              item.upazila,
              item.contract_status
            ])
          )

          setDealerList(bufferDealer)

          console.log('---Dealer buffer--- :', dealerList)
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('dealer token print----- : ', err)
        })
    )
  }

  const handleClearFilters = () => {
    setSearchKey('')
    setFromFilter('')
    setToFilter('')
    setCheckedRecentAlloted(false)
    setCheckedContractExpiry(false)
    setCheckedFamilyMember(false)
    setExcludeDate('')
    setExcludeDateDealerList([])
    setClearSelected([])

    assignDealersNoFilter()
  }

  const storeAllotment = () => {
    const submitData = {
      allotment_date: values.allotmentDate,
      // distance_from_zonal: distanceZonal,
      fiscal_year: '2021-2022',
      zone: zone,
      district: values.district,
      upazila: values.upazila,
      // selectedDealerIds: selectedDealerIds
      selectedDealerIds: mainSelectedDealerIds
    }

    return (
      axios({
        url: `${BASE_URL}/api/allotment/create`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        data: submitData,
        method: 'post'
      })
        .then(response => {
          console.log('response print----- : ', response.data)
          return Router.push('/admin/allotments')
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  let serial = 0

  const handleExcludeDate = e => {
    let bufferDontInclude = []
    let bufferContractExpire = []
    let today = new Date()
    console.log('e.target.value---: ', e.target.value)
    axios({
      url: `${BASE_URL}/api/allotments/exclude-date-dealers?excludeDate=${e.target.value}`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${Cookies.get('cToken')}`
      },
      // data: submitData,
      method: 'get'
    })
      .then(response => {
        // setResDatas(response.data);
        console.log('excludeDate Dealer Data print----- : ', response.data)

        response.data.map(item => {
          // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

          bufferDontInclude.push(item.product_id)

          // const Difference_In_Days =
          //   (new Date(item.product.contract_expiry_date).getTime() -
          //     today.getTime()) /
          //   (1000 * 3600 * 24)

          // console.log('Difference_In_Days----:', Difference_In_Days)

          // if (Difference_In_Days < 30) {
          //   bufferContractExpire.push(item.product_id)
          // }
        })

        setExcludeDateDealerList(bufferDontInclude)

        // setContractExpiryList(bufferContractExpire)
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch(err => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        setExcludeDateDealerList([])
        console.log('Recent Alloted print----- : ', err)
      })
  }

  const dontIncludeFilterOne = filterList => {
    return (
      <>
        {dealerList.map(iteam =>
          mainSelectedDealerIds.includes(iteam[0]) ? null : filterList.includes(
              iteam[0]
            ) ? null : (
            <tr>
              <td>
                <Checkbox
                  checked={selectedDealerIds.includes(iteam[0]) ? true : false}
                  onChange={handleChange}
                  name={iteam[0]}
                  inputProps={{
                    'aria-label': 'primary checkbox'
                  }}
                  // style={{ color: '#4CAF50' }}
                />
              </td>
              <td>{++serial}</td>
              <td>{iteam[0]}</td>
              <td>{iteam[1]}</td>
              <td>{iteam[2]}</td>
              <td>{iteam[3]}</td>
              <td>{iteam[4]}</td>
              <td>{iteam[5]}</td>
            </tr>
          )
        )}
      </>
    )
  }
  const dontIncludeFilterTwo = (filterListFirst, filterListSecond) => {
    return (
      <>
        {dealerList.map(iteam =>
          mainSelectedDealerIds.includes(
            iteam[0]
          ) ? null : filterListFirst.includes(iteam[0]) ||
            filterListSecond.includes(iteam[0]) ? null : (
            <tr>
              <td>
                <Checkbox
                  checked={selectedDealerIds.includes(iteam[0]) ? true : false}
                  onChange={handleChange}
                  name={iteam[0]}
                  inputProps={{
                    'aria-label': 'primary checkbox'
                  }}
                  // style={{ color: '#4CAF50' }}
                />
              </td>
              <td>{++serial}</td>
              <td>{iteam[0]}</td>
              <td>{iteam[1]}</td>
              <td>{iteam[2]}</td>
              <td>{iteam[3]}</td>
              <td>{iteam[4]}</td>
              <td>{iteam[5]}</td>
            </tr>
          )
        )}
      </>
    )
  }
  const dontIncludeFilterThree = (
    filterListFirst,
    filterListSecond,
    filterListThird
  ) => {
    return (
      <>
        {dealerList.map(iteam =>
          mainSelectedDealerIds.includes(
            iteam[0]
          ) ? null : filterListFirst.includes(iteam[0]) ||
            filterListSecond.includes(iteam[0]) ||
            filterListThird.includes(iteam[0]) ? null : (
            <tr>
              <td>
                <Checkbox
                  checked={selectedDealerIds.includes(iteam[0]) ? true : false}
                  onChange={handleChange}
                  name={iteam[0]}
                  inputProps={{
                    'aria-label': 'primary checkbox'
                  }}
                  // style={{ color: '#4CAF50' }}
                />
              </td>
              <td>{++serial}</td>
              <td>{iteam[0]}</td>
              <td>{iteam[1]}</td>
              <td>{iteam[2]}</td>
              <td>{iteam[3]}</td>
              <td>{iteam[4]}</td>
              <td>{iteam[5]}</td>
            </tr>
          )
        )}
      </>
    )
  }

  const assignDealerList = () => {
    if (
      checkedContractExpiry &&
      !checkedRecentAlloted &&
      !checkedFamilyMember
    ) {
      return dontIncludeFilterOne(contractExpiryList)
    } else if (
      checkedFamilyMember &&
      !checkedRecentAlloted &&
      !checkedContractExpiry
    ) {
      let familyDealerTemp = []
      let familyDealerList = []
      dealerList.map(dealer => {
        if (dealer[7]) {
          familyDealerTemp.push(dealer[7])
        }
      })
      dealerList.map(dealer => {
        if (familyDealerTemp.includes(dealer[7])) {
          familyDealerList.push(dealer[0], dealer[7])
        }
      })
      console.log('familyDealerList----:', familyDealerList)

      return dontIncludeFilterOne(familyDealerList)
    } else if (
      checkedRecentAlloted &&
      !checkedContractExpiry &&
      !checkedFamilyMember
    ) {
      return dontIncludeFilterOne(recentAllotedDealerList)
    } else if (
      excludeDateDealerList.length > 0 &&
      !checkedRecentAlloted &&
      !checkedContractExpiry &&
      !checkedFamilyMember
    ) {
      return dontIncludeFilterOne(excludeDateDealerList)
    } else if (
      checkedRecentAlloted &&
      checkedContractExpiry &&
      !checkedFamilyMember
    ) {
      return dontIncludeFilterTwo(recentAllotedDealerList, contractExpiryList)
    } else if (
      !checkedRecentAlloted &&
      checkedContractExpiry &&
      checkedFamilyMember
    ) {
      let familyDealerTemp = []
      let familyDealerList = []
      dealerList.map(dealer => {
        if (dealer[7]) {
          familyDealerTemp.push(dealer[7])
        }
      })
      dealerList.map(dealer => {
        if (familyDealerTemp.includes(dealer[7])) {
          familyDealerList.push(dealer[0], dealer[7])
        }
      })
      return dontIncludeFilterTwo(contractExpiryList, familyDealerList)
    } else if (
      checkedRecentAlloted &&
      !checkedContractExpiry &&
      checkedFamilyMember
    ) {
      let familyDealerTemp = []
      let familyDealerList = []
      dealerList.map(dealer => {
        if (dealer[7]) {
          familyDealerTemp.push(dealer[7])
        }
      })
      dealerList.map(dealer => {
        if (familyDealerTemp.includes(dealer[7])) {
          familyDealerList.push(dealer[0], dealer[7])
        }
      })
      return dontIncludeFilterTwo(recentAllotedDealerList, familyDealerList)
    } else if (
      checkedRecentAlloted &&
      checkedContractExpiry &&
      checkedFamilyMember
    ) {
      let familyDealerTemp = []
      let familyDealerList = []
      dealerList.map(dealer => {
        if (dealer[7]) {
          familyDealerTemp.push(dealer[7])
        }
      })
      dealerList.map(dealer => {
        if (familyDealerTemp.includes(dealer[7])) {
          familyDealerList.push(dealer[0], dealer[7])
        }
      })
      return dontIncludeFilterThree(
        recentAllotedDealerList,
        contractExpiryList,
        familyDealerList
      )
    } else {
      return (
        <>
          {dealerList.map(iteam =>
            mainSelectedDealerIds.includes(iteam[0]) ? null : (
              <tr>
                <td>
                  {/* <button type='button' onClick={handleAdd}>
                                test
                              </button> */}
                  <Checkbox
                    // checked={state.item.dealer_id}
                    // checked={true}
                    checked={
                      selectedDealerIds.includes(iteam[0]) ? true : false
                    }
                    // value={item.dealer_id}
                    // onChange={() => {
                    //   const newTodos = [...todos]
                    //   newTodos.push('ashraf')
                    //   setTodos(newTodos)
                    //   console.log('newTodos--', todos)
                    // }}
                    onChange={handleChange}
                    name={iteam[0]}
                    inputProps={{
                      'aria-label': 'primary checkbox'
                    }}
                    // style={{ color: '#4CAF50' }}
                  />
                </td>
                <td>{++serial}</td>
                <td>{iteam[0]}</td>
                <td>{iteam[1]}</td>
                <td>{iteam[3]}</td>
                <td style={{ color: '#4CAF50' }}>CLEARN</td>
                <td>NULL</td>
              </tr>
            )
          )}
        </>
      )
    }
  }

  return (
    <GridContainer>
      <div>
        <Modal
          aria-labelledby='transition-modal-title'
          aria-describedby='transition-modal-description'
          className={classesModal.modal}
          open={openSaveModal}
          onClose={handleCloseSaveModal}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500
          }}
        >
          <Fade in={openSaveModal}>
            <div className={classesModal.paperSave}>
              <h2 id='transition-modal-title'>Confirm?</h2>
              <p id='transition-modal-description'>
                Are you sure you want to release this allotment?
              </p>
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <Button
                  variant='contained'
                  color='secondary'
                  // className={classes.button}
                  startIcon={<CancelIcon />}
                  onClick={() => handleCloseSaveModal()}
                >
                  Cancel
                </Button>
                <Button
                  variant='contained'
                  color='secondary'
                  // className={classes.button}
                  style={{ background: '#00AC34', marginLeft: '20px' }}
                  startIcon={<SaveIcon />}
                  onClick={() => releaseAllotment()}
                >
                  Yes, I'm sure
                </Button>
              </div>
              <br />
              {console.log('allotmentDealer----', allotmentDealer)}
              <Link
                href={`${BASE_URL}/api/pdf?dealerId=${allotmentDealer?.dealer_id}&allotmentId=${allotmentId}`}
              >
                <a target='_blank'>
                  <Button
                    fullWidth
                    variant='contained'
                    color='secondary'
                    // className={classes.button}
                    style={{
                      background: 'transparent',
                      textTransform: 'none',
                      color: '#00AC34',
                      border: '1px solid #00AC34'
                    }}
                    startIcon={<SendIcon />}
                    onClick={() => {
                      releaseAllotment()
                    }}
                  >
                    DELIVERY ORDER & DOWNLOAD PDF
                  </Button>
                </a>
              </Link>
            </div>
          </Fade>
        </Modal>
      </div>

      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardBody>
            <GridContainer>
              <GridItem xs={12} sm={12} md={6} style={{ marginTop: '10px' }}>
                <div className='dealer_details'>
                  <h3>Allotment #{allotmentId}</h3>
                </div>
              </GridItem>
              <GridItem xs={12} sm={12} md={6} style={{ marginTop: '10px' }}>
                <div style={{ textAlign: 'right' }} className='dealer_details'>
                  {!isReleased ? (
                    <>
                      <Button
                        color='primary'
                        style={{ background: '#4CAF50', marginRight: '30px' }}
                        // onClick={releaseAllotment}
                        onClick={handleOpenSaveModal}
                      >
                        Delivery order
                      </Button>
                      <Link href={`/admin/dealers`}>
                        <Button
                          color='primary'
                          style={{ background: '#898B8A' }}
                          // onClick={() => handleClose()}
                        >
                          Cancel
                        </Button>
                      </Link>
                    </>
                  ) : (
                    <>
                      <Link
                        href={`${BASE_URL}/api/pdf?dealerId=${allotmentDealer?.dealer_id}&allotmentId=${allotmentId}`}
                      >
                        <a target='_blank'>
                          <Button
                            color='primary'
                            style={{
                              background: '#4CAF50',
                              marginRight: '30px'
                            }}
                          >
                            Print PDF
                          </Button>
                        </a>
                      </Link>
                    </>
                  )}
                </div>
              </GridItem>
            </GridContainer>
          </CardBody>
        </Card>
      </GridItem>

      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardBody>
            <GridContainer>
              <GridItem xs={12} sm={12} md={4} style={{ marginTop: '10px' }}>
                <div className='dealer_details'>
                  <>
                    <strong>{allotmentDealer?.name}</strong>
                    <p>{allotmentDealer?.mobile_no}</p>
                    <p>{allotmentDealer?.shop_name}</p>
                  </>
                </div>
              </GridItem>
              <GridItem xs={12} sm={12} md={4} style={{ marginTop: '10px' }}>
                <div style={{ textAlign: 'center' }} className='dealer_details'>
                  <strong>Trading Corporation of Bangladesh</strong>
                  <p>Zonal Office Tejgaon</p>
                  <p>Dhaka</p>
                </div>
              </GridItem>
              <GridItem
                xs={12}
                sm={12}
                md={4}
                style={{
                  marginTop: '10px',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'right'
                }}
              >
                <div style={{ textAlign: 'right' }}>
                  <p>Fiscal year: {fiscalYear}</p>
                </div>
              </GridItem>
            </GridContainer>
          </CardBody>
        </Card>
      </GridItem>

      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardBody>
            <Table
              tableHeaderColor='primary'
              tableHead={[
                'Product name',
                'Allotment amount kg/litre',
                'Price per kg/litre (in taka)',
                'Transport cost (in taka)',
                'Total price (in taka)'
                // 'Actions'
              ]}
              tableData={resDatas}
            />

            <GridContainer>
              <GridItem xs={12} sm={12} md={10}>
                <p style={{ textAlign: 'right', marginRight: '0px' }}>
                  Subtotal :{' '}
                </p>
                <p style={{ textAlign: 'right', marginRight: '0px' }}>
                  According to ordinance, 1984 subsection 53(E) items total
                  price x 5% x 5% deducted from source :{' '}
                </p>
                <p style={{ textAlign: 'right', marginRight: '0px' }}>
                  Grand Total :{' '}
                </p>
              </GridItem>
              <GridItem xs={12} sm={12} md={2}>
                <p style={{ fontWeight: 'bold', marginLeft: '7px' }}>
                  {subTotal}
                </p>
                <p style={{ fontWeight: 'bold', marginLeft: '7px' }}>
                  {subTotal * 0.0025}
                </p>
                <p style={{ fontWeight: 'bold', marginLeft: '7px' }}>
                  {subTotal + subTotal * 0.0025}
                </p>
              </GridItem>
            </GridContainer>
          </CardBody>
          <CardFooter>
            {/* <Button
              color='primary'
              style={{ background: '#4CAF50' }}
              onClick={handleAddClick}
            >
              Add Product
            </Button> */}
          </CardFooter>
        </Card>
      </GridItem>

      <GridItem xs={12} sm={12} md={6}>
        <Card>
          <CardHeader
            plain
            color='primary'
            style={{
              background: '#898b8a',
              boxShadow:
                '0 4px 20px 0 rgb(0 0 0 / 14%), 0 7px 10px -5px rgb(80 78 80 / 40%)'
            }}
          >
            <h4 className={classes.cardTitleWhite}>Payment information</h4>
            {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
          </CardHeader>
          <CardBody>
            <GridContainer>
              <GridItem
                xs={12}
                sm={12}
                md={6}
                style={{ marginTop: '22px', marginBottom: '12px' }}
              >
                <div className='dealer_details'>
                  <p>Paid via: Online Banking</p>
                  <p>Payment date: Sep 14, 2022</p>
                </div>
              </GridItem>
              <GridItem
                xs={12}
                sm={12}
                md={6}
                style={{ marginTop: '22px', marginBottom: '12px' }}
              >
                <div className='dealer_details'>
                  <p>Payment ID: (if any)</p>
                  <p>Trust Bank Ltd. Online</p>
                </div>
              </GridItem>
            </GridContainer>
          </CardBody>
        </Card>
      </GridItem>
      <GridItem xs={12} sm={12} md={6}>
        <Card>
          <CardHeader
            plain
            color='primary'
            style={{
              background: '#898b8a',
              boxShadow:
                '0 4px 20px 0 rgb(0 0 0 / 14%), 0 7px 10px -5px rgb(80 78 80 / 40%)'
            }}
          >
            <h4 className={classes.cardTitleWhite}>Allotment delivery area</h4>
            {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
          </CardHeader>
          <CardBody>
            <GridContainer>
              <GridItem xs={12} sm={12} md={6} style={{ marginTop: '5px' }}>
                <div className='dealer_details'>
                  <p>District: {district}</p>
                  <p>Upazila: {upazila}</p>
                  <p>Distance: {allotmentDealer?.distance_from_zonal} km</p>
                </div>
              </GridItem>
              <GridItem
                xs={12}
                sm={12}
                md={6}
                style={{
                  marginTop: '5px',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'right'
                }}
              >
                <div style={{ textAlign: 'right' }}>
                  <p>Allotment date: {allotmentDate}</p>
                </div>
              </GridItem>
            </GridContainer>
          </CardBody>
        </Card>
      </GridItem>

      {console.log('dealerList----susam: ', dealerList)}
      {dealerList.length > 0 ? (
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader
              plain
              color='primary'
              style={{
                background: '#898b8a',
                boxShadow:
                  '0 4px 20px 0 rgb(0 0 0 / 14%), 0 7px 10px -5px rgb(80 78 80 / 40%)'
              }}
            >
              <h4 className={classes.cardTitleWhite}>Assign Dealers</h4>
              {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
            </CardHeader>
            <CardBody>
              <br />
              <div className={'assign_dealers'}>
                <table>
                  <tr>
                    {/* <th>Select Dealer</th> */}
                    <th>Dealer Id</th>
                    <th>Name</th>
                    <th>Mobile no</th>
                    <th>Shop name</th>
                    <th>Upazila</th>
                    <th>Contract status</th>
                  </tr>

                  {dealerList.map(iteam => (
                    <tr>
                      <td>{iteam[0]}</td>
                      <td>{iteam[1]}</td>
                      <td>{iteam[2]}</td>
                      <td>{iteam[3]}</td>
                      <td>{iteam[4]}</td>
                      <td>{iteam[5]}</td>
                    </tr>
                  ))}
                </table>
              </div>
            </CardBody>
            {removeDealerIds.length > 0 ? (
              <CardFooter>
                {/* <Link href='/admin/add-new-dealer'> */}
                <Button
                  color='primary'
                  style={{ background: '#F0383B' }}
                  onClick={() => {
                    handleRemoveDealers()
                  }}
                >
                  Remove
                </Button>
                <Button
                  color='primary'
                  style={{ background: '#4CAF50' }}
                  onClick={() => handleCancelRemove()}
                >
                  Cancel
                </Button>
                {/* </Link> */}
              </CardFooter>
            ) : null}
          </Card>
        </GridItem>
      ) : null}

      <GridItem xs={12} sm={12} md={12}>
        {/* <br /> */}
        <div style={{ textAlign: 'center' }}>
          {/* <Button
            color='primary'
            onClick={() => {
              assignDealersNoFilter()
              handleOpen()
            }}
            style={{ background: '#4CAF50', textAlign: 'center' }}
            // disabled={!formIsValid()}
          >
            Assign dealers
          </Button> */}
        </div>

        {/* <br /> */}
        {/* <br /> */}
      </GridItem>
      <GridItem xs={12} sm={12} md={12} style={{ marginTop: '0px' }}>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <Link href={`/admin/allotments`}>
            <Button
              color='primary'
              style={{ background: '#898B8A' }}
              // onClick={() => handleClose()}
            >
              Back
            </Button>
          </Link>
        </div>
      </GridItem>
      {console.log('inputList----', inputList)}
    </GridContainer>
  )
}

DetailsAllotment.layout = Admin

export default DetailsAllotment
