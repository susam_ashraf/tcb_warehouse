import React from 'react'
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles'
// layout for this page
import Admin from 'layouts/Admin.js'
// core components
import GridItem from 'components/Grid/GridItem.js'
import GridContainer from 'components/Grid/GridContainer.js'
import Table from 'components/Table/Table.js'
import Card from 'components/Card/Card.js'
import CardHeader from 'components/Card/CardHeader.js'
import CardBody from 'components/Card/CardBody.js'
import CardFooter from 'components/Card/CardFooter.js'
import Button from 'components/CustomButtons/Button.js'
import Link from 'next/link'
import axios from 'axios'
import { useEffect, useState } from 'react'
import { BASE_URL } from '../../env.js'
import Cookies from 'js-cookie'
import moment from 'moment'

import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import DeleteIcon from '@material-ui/icons/Delete'
import CancelIcon from '@material-ui/icons/Cancel'
import Pagination from '@material-ui/lab/Pagination'

import WithAuth from '../../components/WithAuth'

const useStylesModal = makeStyles(theme => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  }
}))

const styles = {
  cardCategoryWhite: {
    '&,& a,& a:hover,& a:focus': {
      color: 'rgba(255,255,255,.62)',
      margin: '0',
      fontSize: '14px',
      marginTop: '0',
      marginBottom: '0'
    },
    '& a,& a:hover,& a:focus': {
      color: '#FFFFFF'
    }
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none',
    '& small': {
      color: '#777',
      fontSize: '65%',
      fontWeight: '400',
      lineHeight: '1'
    }
  }
}

function AllotmentList () {
  const useStyles = makeStyles(styles)
  const classes = useStyles()
  const classesModal = useStylesModal()
  const [open, setOpen] = React.useState(false)
  const [deleteID, setDeleteID] = React.useState(99)

  const [resDatas, setResDatas] = useState([])
  const [refreshDeleteFlag, setRefreshDeleteFlag] = useState(true)
  const [page, setPage] = useState(1)
  const [lastPage, setLastPage] = useState(1)

  let buffer = []

  const handleOpen = id => {
    setOpen(true)
    setDeleteID(id)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handlePaginaton = (event, value) => {
    setPage(value)

    let paginationData = axios({
      url: `${BASE_URL}/api/warehouse/released-allotment-list?page=${value}`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${Cookies.get('cToken')}`
      },
      // data: submitData,
      method: 'get'
    })
      .then(response => {
        // setResDatas(response.data);
        console.log('Pagination data print----- : ', response.data)

        response.data.data.map(item =>
          // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

          buffer.push([
            // <Link href={`/admin/details-dealer?id=${item.id}`} className={stylesCustom.blackC}>
            //   {moment(item.allotment_date).format('MMMM Do YYYY')}
            // </Link>
            item.allotment_id,
            // <Link href={`/admin/details-allotment?id=${item.id}`} className={stylesCustom.blackC}>
            //   {/* <>{moment(item.allotment_date).format('MMMM Do YYYY')}</> */}
            //   susam
            // </Link>,
            // <Link href='/admin/details-dealer'>{item.id}</Link>,
            <Link href={`/admin/details-allotment?id=${item.allotment_id}&dealerId=${item.dealer_id}`}>
              <a>
                {moment(item.allotment.allotment_date).format('MMMM Do YYYY')}
              </a>
            </Link>,
            // moment(item.allotment_date).format('MMMM Do YYYY'),
            item.allotment.fiscal_year,
            item.allotment.district,
            item.allotment.upazila,
            `${item.dealer.distance_from_zonal} km`,
            // <Link href={`/admin/edit-dealer?id=${item.id}`}>
            //   <button class="MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss95 jss111 jss412" tabindex="0" type="button">
            //     <svg style={{ color: '#4caf50'}} class="MuiSvgIcon-root jss413" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34a.9959.9959 0 00-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"></path></svg>
            //   </button>
            // </Link>,
            // <button
            //   onClick={() => deleteAllotment(item.id)}
            //   class='MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss97 jss111 jss412'
            //   tabindex='0'
            //   type='button'
            // >
            //   <svg
            //     style={{ color: '#f44336' }}
            //     class='MuiSvgIcon-root jss413'
            //     focusable='false'
            //     viewBox='0 0 24 24'
            //     aria-hidden='true'
            //   >
            //     <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z'></path>
            //   </svg>
            // </button>
            // <button
            //   onClick={() => handleOpen(item.allotment.id)}
            //   class='MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss97 jss111 jss412'
            //   tabindex='0'
            //   type='button'
            // >
            //   <svg
            //     style={{ color: '#f44336' }}
            //     class='MuiSvgIcon-root jss413'
            //     focusable='false'
            //     viewBox='0 0 24 24'
            //     aria-hidden='true'
            //   >
            //     <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z'></path>
            //   </svg>
            // </button>
          ])
        )
        setResDatas(buffer)

        console.log(resDatas, '---buffer==')

        // return Router.push('/admin/table-list')
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch(err => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        console.log('token print----- : ', err)
      })
  }

  useEffect(() => {
    let initialData = axios({
      url: `${BASE_URL}/api/warehouse/released-allotment-list`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${Cookies.get('cToken')}`
      },
      // data: submitData,
      method: 'get'
    })
      .then(response => {
        // setResDatas(response.data);
        console.log('Data print----- : ', response.data)

        setLastPage(response.data.last_page)

        // response.data.data.map(item =>
        response.data.data.map(item =>
          // <FormControlLabel value={item.id} control={<Radio name={item.value} />} label={item.value} />

          buffer.push([
            // <Link href={`/admin/details-dealer?id=${item.id}`} className={stylesCustom.blackC}>
            //   {moment(item.allotment_date).format('MMMM Do YYYY')}
            // </Link>
            item.allotment_id,
            // <Link href={`/admin/details-allotment?id=${item.id}`} className={stylesCustom.blackC}>
            //   {/* <>{moment(item.allotment_date).format('MMMM Do YYYY')}</> */}
            //   susam
            // </Link>,
            // <Link href='/admin/details-dealer'>{item.id}</Link>,
            <Link href={`/admin/details-allotment?id=${item.allotment_id}&dealerId=${item.dealer_id}`}>
              <a>
                {moment(item?.allotment?.allotment_date).format('MMMM Do YYYY')}
              </a>
            </Link>,
            // moment(item.allotment_date).format('MMMM Do YYYY'),
            item?.allotment?.fiscal_year,
            item?.allotment?.district,
            item?.allotment?.upazila,
            `${item?.dealer?.distance_from_zonal} km`,
            // <Link href={`/admin/edit-dealer?id=${item.id}`}>
            //   <button class="MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss95 jss111 jss412" tabindex="0" type="button">
            //     <svg style={{ color: '#4caf50'}} class="MuiSvgIcon-root jss413" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34a.9959.9959 0 00-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"></path></svg>
            //   </button>
            // </Link>,
            // <button
            //   onClick={() => deleteAllotment(item.id)}
            //   class='MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss97 jss111 jss412'
            //   tabindex='0'
            //   type='button'
            // >
            //   <svg
            //     style={{ color: '#f44336' }}
            //     class='MuiSvgIcon-root jss413'
            //     focusable='false'
            //     viewBox='0 0 24 24'
            //     aria-hidden='true'
            //   >
            //     <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z'></path>
            //   </svg>
            // </button>
            // <button
            //   onClick={() => handleOpen(item.allotment.id)}
            //   class='MuiButtonBase-root MuiButton-root MuiButton-text jss91 jss97 jss111 jss412'
            //   tabindex='0'
            //   type='button'
            // >
            //   <svg
            //     style={{ color: '#f44336' }}
            //     class='MuiSvgIcon-root jss413'
            //     focusable='false'
            //     viewBox='0 0 24 24'
            //     aria-hidden='true'
            //   >
            //     <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z'></path>
            //   </svg>
            // </button>
          ])
        )
        setResDatas(buffer)

        console.log(resDatas, '---buffer==')

        // return Router.push('/admin/table-list')
      })
      // .then((json) => ({
      //   type: 'SUCCESS',
      //   payload: json,
      // }))
      .catch(err => {
        // if (getToken() && err && err.response && err.response.status === 401) {
        //   logOut()
        // } else {
        //   return {
        //     type: 'FAIL',
        //   }
        // }
        console.log('token print----- : ', err)
      })
  }, [refreshDeleteFlag])

  const deleteAllotment = id => {
    return (
      axios({
        url: `${BASE_URL}/api/allotments/delete/${id}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        // data: submitData,
        method: 'DELETE'
      })
        .then(response => {
          console.log('token print----- : ', response.data)
          // return Router.push('/admin/table-list')
          setRefreshDeleteFlag(!refreshDeleteFlag)
          handleClose()
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  return (
    <Admin>
      <GridContainer>
        <Modal
          aria-labelledby='transition-modal-title'
          aria-describedby='transition-modal-description'
          className={classesModal.modal}
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500
          }}
        >
          <Fade in={open}>
            <div className={classesModal.paper}>
              <h2 id='transition-modal-title'>Delete?</h2>
              <p id='transition-modal-description'>
                You can't undo this action.
              </p>
              <Button
                variant='contained'
                color='secondary'
                // className={classes.button}
                startIcon={<CancelIcon />}
                onClick={() => handleClose()}
              >
                Cancel
              </Button>
              <Button
                variant='contained'
                color='secondary'
                // className={classes.button}
                style={{ background: '#ff3a3a', marginLeft: '20px' }}
                startIcon={<DeleteIcon />}
                onClick={() => deleteAllotment(deleteID)}
              >
                Delete
              </Button>
            </div>
          </Fade>
        </Modal>

        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color='primary' style={{ background: '#4CAF50' }}>
              <h4 className={classes.cardTitleWhite}>Allotments</h4>
              <p className={classes.cardCategoryWhite}>
                {/* Here is a subtitle for this table */}
              </p>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor='primary'
                tableHead={[
                  'Allotment no',
                  'Allotment date',
                  'Fiscal year',
                  'District',
                  'Upazila',
                  'Distance'
                ]}
                // tableData={[
                //   ["Dec 10, 2021 ", "2020 - 2021", "Rajshahi", "Tanore", "0-50 km"],
                //   ["Nov 26, 2021 ", "2020 - 2021", "Dinajpur", "Habiganj", "50-100 km"],
                //   ["Nov 26, 2021 ", "2020 - 2021", "Rajshahi", "Bagha", "100-150 km"],
                //   ["Nov 12, 2021 ", "2020 - 2021", "Dinajpur", "Habiganj", "0-50 km"],
                //   ["Oct 09, 2021 ", "2020 - 2021", "Mymensingh", "Mymensingh", "0-50 km"],
                //   ["Sep 20, 2021 ", "2020 - 2021", "Naogaon", "Badalgachi", "50-100 km"],
                //   ["Sep 16, 2021 ", "2020 - 2021", "Dinajpur", "Habiganj", "0-50 km"],

                // ]}
                tableData={resDatas}
              />
              <br />
              <br />
              <Pagination
                count={lastPage}
                page={page}
                onChange={handlePaginaton}
              />
            </CardBody>
            <CardFooter>
              {/* <Link href='/admin/add-new-allotment'>
                <Button color='primary' style={{ background: '#4CAF50' }}>
                  Add Allotment
                </Button>
              </Link> */}
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
    </Admin>
  )
}

// AllotmentList.layout = Admin

export default WithAuth(AllotmentList)
