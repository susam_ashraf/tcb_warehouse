import React, { useState, useEffect } from 'react'
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles'
import InputLabel from '@material-ui/core/InputLabel'
// layout for this page
import Admin from 'layouts/Admin.js'
// core components
import GridItem from 'components/Grid/GridItem.js'
import GridContainer from 'components/Grid/GridContainer.js'
import CustomInput from 'components/CustomInput/CustomInput.js'
import Button from 'components/CustomButtons/Button.js'
import Card from 'components/Card/Card.js'
import CardHeader from 'components/Card/CardHeader.js'
import CardAvatar from 'components/Card/CardAvatar.js'
import CardBody from 'components/Card/CardBody.js'
import CardFooter from 'components/Card/CardFooter.js'
import Link from 'next/link'

import avatar from 'assets/img/faces/marc.jpg'

import TextField from '@material-ui/core/TextField'
import Select from '@material-ui/core/Select'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import MenuItem from '@material-ui/core/MenuItem'
import axios from 'axios'
import Router, { withRouter } from 'next/router'
import { useRouter } from 'next/router'
import Input from '@material-ui/core/Input'
import { BASE_URL } from '../../env.js'
import Cookies from 'js-cookie'
import TextareaAutosize from '@material-ui/core/TextareaAutosize'

import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import SaveIcon from '@material-ui/icons/Save'
import CancelIcon from '@material-ui/icons/Cancel'
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate'

import { districtList, upazilaList } from './constData.json'
import { useFormControls } from '../../components/ContactFormControls/DealerContactFormControls'

import { withStyles } from '@material-ui/core/styles'
import { green } from '@material-ui/core/colors'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank'
import CheckBoxIcon from '@material-ui/icons/CheckBox'
import Favorite from '@material-ui/icons/Favorite'
import FavoriteBorder from '@material-ui/icons/FavoriteBorder'
import FormControl from '@material-ui/core/FormControl'
import SendIcon from '@material-ui/icons/Send'

const GreenCheckbox = withStyles({
  root: {
    color: green[400],
    '&$checked': {
      color: green[600]
    }
  },
  checked: {}
})(props => <Checkbox color='default' {...props} />)

const useStyles = makeStyles(theme => ({
  cardCategoryWhite: {
    color: 'rgba(255,255,255,.62)',
    margin: '0',
    fontSize: '14px',
    marginTop: '0',
    marginBottom: '0'
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none'
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  }
}))

function UserProfile () {
  const {
    handleInputValue,
    handleFormSubmit,
    formIsValid,
    errors,
    values
  } = useFormControls()

  const classes = useStyles()

  const [open, setOpen] = React.useState(false)

  const handleOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  useEffect(() => {
    // districtList.map(item => (
    //   <MenuItem value={item.name} key={item.id}>
    //     {item.name}
    //   </MenuItem>

    // ))

    districtList.forEach(item => {
      if (item.name == values.district) {
        setGenerateDealerId(`01${item.districtCode}`)
      }
    })
  }, [values.district])

  useEffect(() => {
    // {upazilaList.map(item =>
    //   item.district == values.district
    //     ? item.upazilas.map(itemUpz => (
    //         <MenuItem value={itemUpz.name} key={itemUpz.id}>
    //           {itemUpz.name}
    //         </MenuItem>
    //       ))
    //     : null
    // )}

    upazilaList.forEach(item => {
      if (item.district == values.district) {
        item.upazilas.forEach(upazila => {
          if (upazila.name == values.upazila) {
            setGenerateDealerId(`01${item.districtCode}${upazila.upazilaCode}`)

            dealerIdGeneration(`01${item.districtCode}${upazila.upazilaCode}`)
          }
        })
      }
    })
  }, [values.upazila])

  useEffect(() => {
    return (
      axios({
        url: `${BASE_URL}/api/dealer/${values.familyDealerId}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        method: 'get'
      })
        .then(response => {
          if (response.data) {
            let family_dealer_name = response.data.name

            console.log('dealerName print----- : ', family_dealer_name)

            setFamilyDealerName(family_dealer_name)
          } else {
            console.log('success false---', response.data.success)
          }
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('error print----- : ', err)
        })
    )
  }, [values.familyDealerId])

  const { query } = useRouter()

  const [name, setName] = useState()
  const [shopName, setShopName] = useState()
  const [mobileNo, setMobileNo] = useState()
  const [zone, setZone] = useState('Dhaka')
  const [district, setDistrict] = useState()
  const [upazila, setUpazila] = useState()
  const [distanceZonal, setDistanceZonal] = useState()
  const [shopAddress, setShopAddress] = useState()
  const [contractStatus, setContractStatus] = useState()
  const [relationType, setRelationType] = useState('son')
  const [contractRenewalDate, setContractRenewalDate] = useState()
  const [familyDealerId, setFamilyDealerId] = useState()
  const [limitNumber, setLimitNumber] = useState()
  const [generateDealerId, setGenerateDealerId] = useState()
  const [familyDealerName, setFamilyDealerName] = useState()
  const [statusComment, setStatusComment] = useState()
  const [selectedImage, setSelectedImage] = useState()
  const [isOverSizeImage, setIsOverSizeImage] = useState(false)
  const [imageDimensions, setImageDimensions] = useState({})

  const storeDealer = () => {
    const submitData = {
      dealer_id: generateDealerId,
      name: values.dealerName,
      shop_name: values.shopName,
      distance_from_zonal: values.distanceZonal,
      shop_address: values.shopAddress,
      mobile_no: values.mobileNo,
      zone: zone,
      district: values.district,
      upazila: values.upazila,
      contract_status: values.contractStatus,
      show_cause: state.checkedA,
      forgery_proved: state.checkedB,
      flagged: state.checkedC,
      comment: statusComment,
      contract_date: values.contractDate,
      contract_expiry_date: values.contractExpiryDate,
      contract_renewal_date: contractRenewalDate,
      family_dealer_id: values.familyDealerId,
      family_relation_type: values.relationType,
      selectedImage: selectedImage
    }

    const bodyFormData = new FormData()

    bodyFormData.append('dealer_id', generateDealerId)
    bodyFormData.append('name', values.dealerName)
    bodyFormData.append('shop_name', values.shopName)
    bodyFormData.append('distance_from_zonal', values.distanceZonal)
    bodyFormData.append('shop_address', values.shopAddress)
    bodyFormData.append('mobile_no', values.mobileNo)
    bodyFormData.append('zone', zone)
    bodyFormData.append('district', values.district)
    bodyFormData.append('upazila', values.upazila)
    bodyFormData.append('contract_status', values.contractStatus)
    bodyFormData.append('show_cause', state.checkedA)
    bodyFormData.append('forgery_proved', state.checkedB)
    bodyFormData.append('flagged', state.checkedC)
    bodyFormData.append('comment', statusComment)
    bodyFormData.append('contract_date', values.contractDate)
    bodyFormData.append('contract_expiry_date', values.contractExpiryDate)
    bodyFormData.append('contract_renewal_date', contractRenewalDate)
    bodyFormData.append('family_dealer_id', values.familyDealerId)
    bodyFormData.append('family_relation_type', values.relationType)
    bodyFormData.append('selectedImage', selectedImage)

    return (
      axios({
        url: `${BASE_URL}/api/create`,
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        data: bodyFormData,
        method: 'post'
      })
        .then(response => {
          console.log('token print----- : ', response.data)
          return Router.push('/admin/dealers')
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  const sendSmsDealer = () => {
    return (
      axios({
        url: `${BASE_URL}/api/send-sms?phone=${values.mobileNo}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        method: 'get'
      })
        .then(response => {
          console.log('sms resp----', response)
          return false
          // return response
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  const dealerIdGeneration = partialId => {
    return (
      axios({
        url: `${BASE_URL}/api/last-dealer-id/${partialId}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        method: 'get'
      })
        .then(response => {
          if (response.data.success) {
            let dealerIdData = response.data.data + 1

            console.log(
              'last dealerID print-----ff : ',
              dealerIdData.toString().charAt(0)
            )

            if (dealerIdData.toString().charAt(0) == 0) {
              setGenerateDealerId(`${dealerIdData}`)
            } else {
              setGenerateDealerId(`0${dealerIdData}`)
            }
          } else {
            console.log('success false---', response.data.success)

            setGenerateDealerId(`${partialId}001`)
          }
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('token print----- : ', err)
        })
    )
  }

  const getDealerName = () => {
    return (
      axios({
        url: `${BASE_URL}/api/dealer/${values.familyDealerId}`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${Cookies.get('cToken')}`
        },
        method: 'get'
      })
        .then(response => {
          if (response.data) {
            let family_dealer_name = response.data.name

            console.log('dealerName print----- : ', family_dealer_name)

            setFamilyDealerName(family_dealer_name)
          } else {
            console.log('success false---', response.data.success)
          }
        })
        // .then((json) => ({
        //   type: 'SUCCESS',
        //   payload: json,
        // }))
        .catch(err => {
          // if (getToken() && err && err.response && err.response.status === 401) {
          //   logOut()
          // } else {
          //   return {
          //     type: 'FAIL',
          //   }
          // }
          console.log('error print----- : ', err)
        })
    )
  }

  // for checkbox----

  const [state, setState] = React.useState({
    checkedA: false,
    checkedB: false,
    checkedC: false,
    checkedG: false
  })

  // const [checkShowCause, setCheckShowCause] = useState(true)

  const handleChange = event => {
    setState({ ...state, [event.target.name]: event.target.checked })
  }

  // end for checkbox----

  return (
    <div>
      <GridContainer>
        <div>
          {console.log(
            'form values------ :',
            values,
            '1st----: ',
            values.mobileNo.charAt(0),
            '2nd--- :',
            values.mobileNo.charAt(1)
          )}
          {console.log('typeof---- : ', typeof generateDealerId)}
          <Modal
            aria-labelledby='transition-modal-title'
            aria-describedby='transition-modal-description'
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500
            }}
          >
            <Fade in={open}>
              <div className={classes.paper}>
                <h2 id='transition-modal-title'>Confirm?</h2>
                <p id='transition-modal-description'>
                  Save all the information & add the dealer.
                </p>
                <div
                  style={{ display: 'flex', justifyContent: 'space-between' }}
                >
                  <Button
                    variant='contained'
                    color='secondary'
                    // className={classes.button}
                    startIcon={<CancelIcon />}
                    onClick={() => handleClose()}
                  >
                    Cancel
                  </Button>
                  <Button
                    variant='contained'
                    color='secondary'
                    // className={classes.button}
                    style={{ background: '#00AC34', marginLeft: '20px' }}
                    startIcon={<SaveIcon />}
                    onClick={() => storeDealer()}
                  >
                    Save
                  </Button>
                </div>
                <Button
                  fullWidth
                  variant='contained'
                  color='secondary'
                  // className={classes.button}
                  style={{
                    background: 'transparent',
                    textTransform: 'none',
                    color: '#00AC34',
                    border: '1px solid #00AC34',
                    marginTop: '15px'
                  }}
                  startIcon={<SendIcon />}
                  onClick={() => {
                    sendSmsDealer()
                    storeDealer()
                  }}
                >
                  Send Confirmation SMS & save
                </Button>
              </div>
            </Fade>
          </Modal>
        </div>

        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color='primary' style={{ background: '#4CAF50' }}>
              <h4 className={classes.cardTitleWhite}>Add New Dealer</h4>
              {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
            </CardHeader>
            <CardBody>
              <br />
              {/* <h4 style={{ marginBottom: '10px' }}>Dealer Id : {generateDealerId}</h4> */}
              {/* <br />
              <br /> */}
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <div
                    style={{ display: 'flex', justifyContent: 'space-between' }}
                  >
                    <div>
                      {selectedImage && (
                        <div>
                          <img
                            src={URL.createObjectURL(selectedImage)}
                            style={{
                              width: '150px',
                              height: '150px',
                              marginBottom: '20px',
                              borderRadius: '50%'
                            }}
                            alt='Thumb'
                          />
                          {/* <button onClick={removeSelectedImage} style={styles.delete}>
                                Remove This Image
                              </button> */}
                        </div>
                      )}
                    </div>
                    <div
                      style={{
                        display: 'flex',
                        justifyContent: 'right'
                      }}
                    >
                      {state.checkedA ? (
                        <div
                          style={{
                            width: '20px',
                            height: '20px',
                            background: '#EA80FC',
                            marginBottom: '20px',
                            borderRadius: '2px',
                            float: 'right',
                            marginRight: '20px'
                          }}
                          className='show_cause_box'
                        ></div>
                      ) : null}

                      {state.checkedB ? (
                        <div
                          style={{
                            width: '20px',
                            height: '20px',
                            background: '#E65101',
                            marginBottom: '20px',
                            borderRadius: '2px',
                            float: 'right',
                            marginRight: '20px'
                          }}
                          className='show_cause_box'
                        ></div>
                      ) : null}

                      {state.checkedC ? (
                        <div
                          style={{
                            width: '20px',
                            height: '20px',
                            background: '#DE3933',
                            marginBottom: '20px',
                            borderRadius: '2px',
                            float: 'right'
                          }}
                          className='show_cause_box'
                        ></div>
                      ) : null}
                    </div>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={12}>
                  <input
                    accept='image/*'
                    className={classes.input}
                    id='contained-button-file'
                    multiple
                    type='file'
                    style={{ display: 'none' }}
                    onChange={e => {
                      // setSelectedImage(e.target.files[0])

                      let img = new Image()
                      img.src = URL.createObjectURL(e.target.files[0])
                      //   img.onload = function() {
                      //     // alert(this.width + " " + this.height);
                      //     setImageDimensions({
                      //       height: img.height,
                      //       width: img.width
                      //     })
                      // };
                      img.onload = () => {
                        // alert(img.width + " " + img.height)
                        setImageDimensions({
                          height: img.height,
                          width: img.width
                        })
                        if (img.height > 3000 && img.width > 3000) {
                          e.target.value = null
                          setSelectedImage()
                          setIsOverSizeImage(true)
                        } else {
                          setSelectedImage(e.target.files[0])
                          setIsOverSizeImage(false)
                        }
                      }

                      // if(imageDimensions.height > 500){
                      //   e.target.value = null
                      // }

                      // setTimeout(() => {
                      //   e.target.value = null
                      // }, 2000)
                    }}
                  />
                  <label htmlFor='contained-button-file'>
                    {/* <Button
                      variant='contained'
                      color='primary'
                      component='span'
                    >
                      Upload
                    </Button> */}
                    <Button
                      variant='contained'
                      color='default'
                      component='span'
                      // className={classes.button}
                      startIcon={<AddPhotoAlternateIcon />}
                      style={{ background: '#4CAF50' }}
                    >
                      Add image
                    </Button>
                  </label>
                  {/* <InputLabel htmlFor='component-helper'>Add image</InputLabel>
                  <Input
                    fullWidth
                    id='component-helper'
                    // value={this.state.name}
                    // onChange={this.handleChange}
                    aria-describedby='component-helper-text'
                    type='file'
                    onChange={e => {
                      // setSelectedImage(e.target.files[0])

                      let img = new Image()
                      img.src = URL.createObjectURL(e.target.files[0])
                      //   img.onload = function() {
                      //     // alert(this.width + " " + this.height);
                      //     setImageDimensions({
                      //       height: img.height,
                      //       width: img.width
                      //     })
                      // };
                      img.onload = () => {
                        // alert(img.width + " " + img.height)
                        setImageDimensions({
                          height: img.height,
                          width: img.width
                        })
                        if (img.height > 3000 && img.width > 3000) {
                          e.target.value = null
                          setSelectedImage()
                          setIsOverSizeImage(true)
                        } else {
                          setSelectedImage(e.target.files[0])
                          setIsOverSizeImage(false)
                        }
                      }

                      // if(imageDimensions.height > 500){
                      //   e.target.value = null
                      // }

                      // setTimeout(() => {
                      //   e.target.value = null
                      // }, 2000)
                    }}
                  /> */}
                  <p
                    {...(isOverSizeImage && {
                      style: { color: 'red' }
                    })}
                  >
                    Please add an image of maximum 300x300 pixel.
                  </p>
                  <br />
                </GridItem>
                <br />
                <br />
                <br />
                <br />
              </GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  <TextField
                    InputLabelProps={{ shrink: true }}
                    id='standard-basic'
                    label='Dealer Id'
                    variant='outlined'
                    disabled
                    value={generateDealerId}
                    fullWidth
                    // onChange={e => setZone(e.target.value)}
                  />{' '}
                  <br /> <br />
                </GridItem>

                <GridItem xs={12} sm={12} md={4}>
                  <TextField
                    id='standard-basic'
                    label='Dealer name'
                    variant='outlined'
                    fullWidth
                    // onChange={e => setName(e.target.value)}
                    onChange={handleInputValue}
                    onBlur={handleInputValue}
                    name='dealerName'
                    error={errors['dealerName']}
                    {...(errors['dealerName'] && {
                      error: true,
                      helperText: errors['dealerName']
                    })}
                  />{' '}
                  <br /> <br />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <TextField
                    id='standard-basic'
                    label='Shop name'
                    variant='outlined'
                    fullWidth
                    // onChange={e => setShopName(e.target.value)}
                    onChange={handleInputValue}
                    onBlur={handleInputValue}
                    name='shopName'
                    error={errors['shopName']}
                    {...(errors['shopName'] && {
                      error: true,
                      helperText: errors['shopName']
                    })}
                  />{' '}
                  <br /> <br />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <TextField
                    id='standard-basic'
                    label='Shop address'
                    variant='outlined'
                    fullWidth
                    // onChange={e => setShopAddress(e.target.value)}
                    onChange={handleInputValue}
                    onBlur={handleInputValue}
                    name='shopAddress'
                    error={errors['shopAddress']}
                    {...(errors['shopAddress'] && {
                      error: true,
                      helperText: errors['shopAddress']
                    })}
                  />{' '}
                  <br /> <br />
                  {/* <TextareaAutosize fullWidth aria-label="minimum height" minRows={5} placeholder="Minimum 3 rows" /> */}
                  {/* <textarea rows='3' cols='42' name='comment' form='usrform'>
                    Enter text here...
                  </textarea> */}
                </GridItem>

                <GridItem xs={12} sm={12} md={4}>
                  {/* <Input
                    type='number'
                    value={limitNumber}
                    label='Number'
                    hint='Enter Your Favourite Number'
                    onChange={e => setLimitNumber(e.target.value)}
                    error={
                      limitNumber.length > 12
                        ? 'Enter a number less than 12'
                        : ''
                    }
                    max={12}
                  /> */}
                  {/* <input
                    type='number'
                    name='phone'
                    value={limitNumber}
                    onChange={event => {
                      if (event.target.value.length == 11) return false //limits to 10 digit entry
                      setLimitNumber(event?.target.value) //saving input to state
                    }}
                    id='tel'
                    placeholder='Your phone no (10 digit)'
                  /> */}
                  <TextField
                    required
                    type='number'
                    max='11'
                    id='standard-basic'
                    label='Mobile number'
                    variant='outlined'
                    fullWidth
                    // onChange={e => setMobileNo(e.target.value)}
                    onChange={handleInputValue}
                    // onChange={event => {
                    //   if (event.target.value.length > 5) return false //limits to 10 digit entry
                    //   handleInputValue //saving input to state
                    // }}
                    onBlur={handleInputValue}
                    name='mobileNo'
                    error={errors['mobileNo']}
                    {...(errors['mobileNo'] && {
                      error: true,
                      helperText: errors['mobileNo']
                    })}
                  />{' '}
                  <br /> <br />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <TextField
                    InputLabelProps={{ shrink: true }}
                    id='standard-basic'
                    label='Zone'
                    variant='outlined'
                    disabled
                    value={zone}
                    fullWidth
                    onChange={e => setZone(e.target.value)}
                  />{' '}
                  <br /> <br />
                </GridItem>
              </GridContainer>

              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  <FormControl
                    fullWidth
                    variant='outlined'
                    className={classes.formControl}
                  >
                    <InputLabel
                      style={{ marginBottom: '10px' }}
                      htmlFor='outlined-age-simple'
                    >
                      District
                    </InputLabel>
                    <Select
                      label='District'
                      fullWidth
                      value={values.district}
                      // onChange={e => setDistrict(e.target.value)}
                      onChange={handleInputValue}
                      onBlur={handleInputValue}
                      name='district'
                      error={errors['district']}
                      {...(errors['district'] && {
                        error: true,
                        helperText: errors['district']
                      })}
                      inputProps={{
                        name: 'district',
                        id: 'outlined-age-native-simple'
                      }}
                    >
                      {districtList.map(item => (
                        <MenuItem value={item.name} key={item.id}>
                          {item.name}
                        </MenuItem>
                      ))}

                      {/* <MenuItem value={'dhaka'}>Dhaka</MenuItem>
                    <MenuItem value={'rajshahi'}>Rajshahi</MenuItem>
                    <MenuItem value={'dinajpur'}>Dinajpur</MenuItem>
                    <MenuItem value={'mymensingh'}>Mymensingh</MenuItem>
                    <MenuItem value={'naogaon'}>Naogaon</MenuItem>
                    <MenuItem value={'faridpur'}>Faridpur</MenuItem> */}
                    </Select>
                  </FormControl>
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <FormControl
                    fullWidth
                    variant='outlined'
                    className={classes.formControl}
                  >
                    <InputLabel
                      style={{ marginBottom: '10px' }}
                      htmlFor='outlined-age-simple'
                    >
                      Upazila
                    </InputLabel>
                    <Select
                      label='Upazila'
                      fullWidth
                      value={values.upazila}
                      // onChange={e => setUpazila(e.target.value)}
                      onChange={handleInputValue}
                      onBlur={handleInputValue}
                      name='upazila'
                      error={errors['upazila']}
                      {...(errors['upazila'] && {
                        error: true,
                        helperText: errors['upazila']
                      })}
                      inputProps={{
                        name: 'upazila',
                        id: 'outlined-age-native-simple'
                      }}
                    >
                      <MenuItem value={''}>Select</MenuItem>
                      {upazilaList.map(item =>
                        item.district == values.district
                          ? item.upazilas.map(itemUpz => (
                              <MenuItem value={itemUpz.name} key={itemUpz.id}>
                                {itemUpz.name}
                              </MenuItem>
                            ))
                          : null
                      )}

                      {/* <MenuItem value={'adamdighi'}>Adamdighi</MenuItem>
                    <MenuItem value={'habiganj'}>Habiganj</MenuItem>
                    <MenuItem value={'bagha'}>Bagha</MenuItem>
                    <MenuItem value={'mymensingh'}>Mymensingh</MenuItem>
                    <MenuItem value={'mohadevpur'}>Mohadevpur</MenuItem>
                    <MenuItem value={'tanore'}>Tanore</MenuItem>
                    <MenuItem value={'badalgachi'}>Badalgachi</MenuItem> */}
                    </Select>
                  </FormControl>
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <FormControl
                    fullWidth
                    variant='outlined'
                    className={classes.formControl}
                  >
                    <InputLabel
                      style={{ marginBottom: '10px' }}
                      htmlFor='outlined-age-simple'
                    >
                      Distance From Zonal office
                    </InputLabel>
                    <Select
                      label='Distance From Zonal office'
                      fullWidth
                      value={values.distanceZonal}
                      // onChange={e => setDistanceZonal(e.target.value)}
                      onChange={handleInputValue}
                      onBlur={handleInputValue}
                      name='distanceZonal'
                      error={errors['distanceZonal']}
                      {...(errors['distanceZonal'] && {
                        error: true,
                        helperText: errors['distanceZonal']
                      })}
                      inputProps={{
                        name: 'distanceZonal',
                        id: 'outlined-age-native-simple'
                      }}
                    >
                      <MenuItem value={'0-50'}>0 - 50 km</MenuItem>
                      <MenuItem value={'51-100'}>51 - 100 km</MenuItem>
                      <MenuItem value={'100+'}>100+ km</MenuItem>
                    </Select>
                  </FormControl>
                </GridItem>
              </GridContainer>

              <GridContainer>
                <GridItem xs={12} sm={12} md={3} style={{ marginTop: '27px' }}>
                  <TextField
                    id='date'
                    label='Contract date'
                    fullWidth
                    type='date'
                    variant='outlined'
                    // defaultValue='2022-01-02'
                    onChange={handleInputValue}
                    onBlur={handleInputValue}
                    name='contractDate'
                    error={errors['contractDate']}
                    {...(errors['contractDate'] && {
                      error: true,
                      helperText: errors['contractDate']
                    })}
                    className={classes.textField}
                    InputLabelProps={{
                      shrink: true
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={3} style={{ marginTop: '27px' }}>
                  <TextField
                    id='date'
                    label='Contract renewal date'
                    fullWidth
                    type='date'
                    variant='outlined'
                    // defaultValue='2022-01-02'
                    className={classes.textField}
                    InputLabelProps={{
                      shrink: true
                    }}
                    onChange={e => setContractRenewalDate(e.target.value)}
                    // onChange={handleInputValue}
                    // onBlur={handleInputValue}
                    // name='contractRenewalDate'
                    // error={errors['contractRenewalDate']}
                    // {...(errors['contractRenewalDate'] && {
                    //   error: true,
                    //   helperText: errors['contractRenewalDate']
                    // })}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={3} style={{ marginTop: '27px' }}>
                  <TextField
                    id='date'
                    label='Contract expiry date'
                    fullWidth
                    type='date'
                    variant='outlined'
                    // defaultValue='2022-01-02'
                    className={classes.textField}
                    InputLabelProps={{
                      shrink: true
                    }}
                    onChange={handleInputValue}
                    onBlur={handleInputValue}
                    name='contractExpiryDate'
                    error={errors['contractExpiryDate']}
                    {...(errors['contractExpiryDate'] && {
                      error: true,
                      helperText: errors['contractExpiryDate']
                    })}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={3} style={{ marginTop: '27px' }}>
                  <FormControl
                    fullWidth
                    variant='outlined'
                    className={classes.formControl}
                  >
                    <InputLabel
                      style={{ marginBottom: '10px' }}
                      htmlFor='outlined-age-simple'
                    >
                      Contract Status
                    </InputLabel>
                    <Select
                      label='Contract Status'
                      fullWidth
                      // value={{
                      //   age: '',
                      //   name: 'hai',
                      //   labelWidth: 0,
                      // }}
                      value={values.contractStatus}
                      // onChange={e => setContractStatus(e.target.value)}
                      onChange={handleInputValue}
                      onBlur={handleInputValue}
                      name='contractStatus'
                      error={errors['contractStatus']}
                      {...(errors['contractStatus'] && {
                        error: true,
                        helperText: errors['contractStatus']
                      })}
                      // input={
                      //   <OutlinedInput
                      //     // labelWidth={100}
                      //     name='age'
                      //     id='outlined-age-simple'
                      //   />
                      // }
                      inputProps={{
                        name: 'contractStatus',
                        id: 'outlined-age-native-simple'
                      }}
                    >
                      {/* <MenuItem value="">
                      <em>None</em>
                    </MenuItem> */}
                      <MenuItem value={'permanent'}>Permanent</MenuItem>
                      <MenuItem value={'new_dealer'}>New dealer</MenuItem>
                      {/* <MenuItem value={30}>Thirty</MenuItem> */}
                    </Select>
                  </FormControl>
                </GridItem>
              </GridContainer>

              {/* <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <InputLabel style={{ color: "#AAAAAA" }}>About me</InputLabel>
                  <CustomInput
                    labelText="Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo."
                    id="about-me"
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      multiline: true,
                      rows: 5,
                    }}
                  />
                </GridItem>
              </GridContainer> */}
            </CardBody>
            <CardFooter>
              {/* <Button
                type='submit'
                color='primary'
                // onClick={() => storeDealer()}
                onClick={handleOpen}
                style={{ background: '#4CAF50' }}
              >
                Save dealer
              </Button> */}
            </CardFooter>
          </Card>
        </GridItem>

        {/* start Family relation section */}
        <GridItem xs={12} sm={12} md={6}>
          <Card>
            <CardHeader
              plain
              color='primary'
              style={{
                background: '#898b8a',
                boxShadow:
                  '0 4px 20px 0 rgb(0 0 0 / 14%), 0 7px 10px -5px rgb(80 78 80 / 40%)'
              }}
            >
              <h4 className={classes.cardTitleWhite}>Family relation</h4>
              {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
            </CardHeader>
            <CardBody>
              <br />
              <br />
              <GridContainer></GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={6}>
                  <TextField
                    id='dealerId'
                    label='Dealer ID'
                    fullWidth
                    type='number'
                    variant='outlined'
                    value={values.familyDealerId}
                    name='familyDealerId'
                    // defaultValue='2021-12-09'
                    className={classes.textField}
                    // onChange={e => setFamilyDealerId(e.target.value)}
                    onChange={handleInputValue}
                    onBlur={getDealerName}
                    // InputLabelProps={{
                    //   shrink: true
                    // }}
                  />
                </GridItem>

                <GridItem xs={12} sm={12} md={6}>
                  <FormControl
                    fullWidth
                    variant='outlined'
                    className={classes.formControl}
                  >
                    <InputLabel
                      style={{ marginBottom: '10px' }}
                      htmlFor='outlined-age-simple'
                    >
                      Relation type
                    </InputLabel>
                    <Select
                      label='Relation type'
                      fullWidth
                      // value={relationType}
                      name='relationType'
                      // {...(familyDealerId ? (onChange={handleInputValue},
                      //   value=values.relationType,
                      //   onBlur=handleInputValue,
                      //   error=errors['relationType'],
                      //   {...(errors['relationType'] && {
                      //     error: true,
                      //     helperText: errors['relationType']
                      //   })}) : {...(
                      //     // onChange=(e => setRelationType(e.target.value),
                      //     value={relationType}
                      //  )} )}

                      {...(values.familyDealerId
                        ? {
                            value: values.relationType,
                            onChange: handleInputValue,
                            onBlur: handleInputValue,
                            error: errors['relationType'],
                            ...(errors['relationType'] && {
                              error: true,
                              helperText: errors['relationType']
                            })
                          }
                        : { disabled: true })}
                      inputProps={{
                        name: 'relationType',
                        id: 'outlined-age-native-simple'
                      }}
                    >
                      <MenuItem value={'father'}>Father</MenuItem>
                      <MenuItem value={'mother'}>Mother</MenuItem>
                      <MenuItem value={'son'}>Son</MenuItem>
                      <MenuItem value={'brother'}>Brother</MenuItem>
                      <MenuItem value={'daughter'}>Daughter</MenuItem>
                      <MenuItem value={'sister'}>Sister</MenuItem>
                      <MenuItem value={'uncle'}>Uncle</MenuItem>
                      <MenuItem value={'cousin'}>Cousin</MenuItem>
                    </Select>
                  </FormControl>
                </GridItem>

                <GridItem xs={12} sm={12} md={12} style={{ marginTop: '27px' }}>
                  <TextField
                    id='standard-basic'
                    label='Dealer Name'
                    variant='outlined'
                    value={familyDealerName}
                    fullWidth
                    disabled
                    // onChange={e => setZone(e.target.value)}
                    InputLabelProps={{ shrink: true }}
                  />{' '}
                  <br /> <br />
                </GridItem>
              </GridContainer>
            </CardBody>
            <CardFooter></CardFooter>
          </Card>
        </GridItem>

        {/* start Dealer status section */}

        <GridItem xs={12} sm={12} md={6}>
          <Card>
            <CardHeader
              plain
              color='primary'
              style={{
                background: '#898b8a',
                boxShadow:
                  '0 4px 20px 0 rgb(0 0 0 / 14%), 0 7px 10px -5px rgb(80 78 80 / 40%)'
              }}
            >
              <h4 className={classes.cardTitleWhite}>Dealer status</h4>
              {/* <p className={classes.cardCategoryWhite}>Complete your profile</p> */}
            </CardHeader>
            <CardBody>
              <br />
              <GridContainer></GridContainer>
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <FormGroup row>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={state.checkedA}
                          onChange={handleChange}
                          // onChange={e => setCheckShowCause(e.target.checked)}
                          name='checkedA'
                          style={{ color: '#EA80FC' }}
                        />
                      }
                      label='SHOW CAUSE'
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={state.checkedB}
                          onChange={handleChange}
                          name='checkedB'
                          style={{ color: '#E65101' }}
                        />
                      }
                      label='FORGERY PROVED'
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={state.checkedC}
                          onChange={handleChange}
                          name='checkedC'
                          style={{ color: '#DE3933' }}
                        />
                      }
                      label='FLAGGED'
                      // label={<SaveIcon />}
                    />
                  </FormGroup>
                </GridItem>
                <GridItem xs={12} sm={12} md={12} style={{ marginTop: '27px' }}>
                  <TextField
                    id='outlined-multiline-static'
                    label='Comment'
                    multiline
                    fullWidth
                    rows={4}
                    // defaultValue='Default Value'
                    onChange={e => setStatusComment(e.target.value)}
                    variant='outlined'
                  />
                  <br />
                </GridItem>
              </GridContainer>
            </CardBody>
            <CardFooter></CardFooter>
          </Card>
        </GridItem>

        <GridItem xs={12} sm={12} md={12} style={{ marginTop: '20px' }}>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <Button
              type='submit'
              color='primary'
              // onClick={() => storeDealer()}
              onClick={handleOpen}
              style={{ background: '#4CAF50' }}
              // disabled={!formIsValid()}
            >
              Save dealer
            </Button>
            <Link href={`/admin/dealers`}>
              <Button
                color='primary'
                style={{ background: '#898B8A' }}
                // onClick={() => handleClose()}
              >
                Back
              </Button>
            </Link>
          </div>
        </GridItem>
      </GridContainer>
    </div>
  )
}

UserProfile.layout = Admin

export default UserProfile
